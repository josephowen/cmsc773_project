This program calculates word clusters based on word2vec and calcuates Long Short Term Memory Recurrent
Neural Network to classify each post as depressed or non-depressed
The following programs are need to run this:
python numpy/scipy
BLAS
python theano - pip install theano
word2vec - http://word2vec.googlecode.com/svn/trunk/
ark-tweet-nlp - https://code.google.com/p/ark-tweet-nlp/downloads/detail?name=ark-tweet-nlp-0.3.2.tgz&can=2&q=

Then one must edit the following:
twitter_LOC =
text_data_LOC='/home/sean/deep/project_materials/mypersonality_depression/text/'
word2vec_LOC='/home/sean/deep/trunk/'
user_data_LOC='/home/sean/deep/cmsc773_project/'
pickle_file_LOC='/home/sean/deep/constr/'

