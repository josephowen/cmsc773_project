import numpy as np
import cPickle
import os.path
import subprocess
import sys
import argparse
import hashlib

twitter_LOC = '/home/sean/deep/ark-tweet-nlp-0.3.2/'
text_data_LOC='/home/sean/deep/project_materials/mypersonality_depression/text/'
word2vec_LOC='/home/sean/deep/trunk/'
user_data_LOC='/home/sean/deep/cmsc773_project/'
pickle_file_LOC='/home/sean/deep/constr/'

avg_missing_n = 0
avg_missing = 0
def mini_print(text):
	sys.stdout.write(str(text))
	sys.stdout.flush()

def load_convert_table(run_name):
	f=open(text_data_LOC+run_name+'.sorted.out','rb')
	mapp = {}
	for line in f:
		s=line.strip().split(' ')
		hashedS = int(hashlib.sha1(s[0]).hexdigest(), 16)
		mapp[hashedS] = int(s[1])
	f.close()
	return mapp
	
def convert_word_list(word_list,word_map):
	global avg_missing
	global avg_missing_n
	N=len(word_list)
	vc = []
	i = 0
	j = 0
	for word in word_list:
		hashedW = int(hashlib.sha1(word).hexdigest(), 16)
		try:
			vc.append(word_map[hashedW])
			j=j+1
		except:
			i=i+1
	if i>1:
		avg_missing = avg_missing+i/float(i+j)
	avg_missing_n = avg_missing_n +1	
	return vc
	
def load_info():
	if not os.path.isfile(pickle_file_LOC+'usermap.pkl'):
		print "Creating usermap.pkl..."
		user_cesd_map = {}
		user_train = []
		user_test = []
		user_depressed = []
		user_nondepressed = []
		f=open(user_data_LOC+'users.csv','rb')
		i =0
		for line in f:
			if i>0:
				cats=line.split(',')
				user_cesd_map[cats[0]]=int(cats[1])
			i=i+1
		print str(i)+": Total Users"
		f.close()
		f=open(user_data_LOC+'users.test.csv','rb')
		i=0
		for line in f:
			if i>0:
				cats=line.split(',')
				user_test.append(cats[0])
			i=i+1
		print str(i)+": Testing Users"
		f.close()
		user_test_set = set(user_test)
		del user_test
		f=open(user_data_LOC+'users.train.csv','rb')
		i=0
		for line in f:
			if i>0:
				cats=line.split(',')
				user_train.append(cats[0])
			i=i+1
		print str(i)+": Training Users"
		f.close()
		user_train_set = set(user_train)
		del user_train
		f=open(user_data_LOC+'users.depressed.csv','rb')
		i=0
		for line in f:
			if i>0:
				cats=line.split(',')
				user_depressed.append(cats[0])
			i=i+1
		print str(i)+": Depressed Users"
		f.close()
		user_depressed_set = set(user_depressed)
		del user_depressed
		f=open(user_data_LOC+'users.nondepressed.csv','rb')
		i=0
		for line in f:
			if i>0:
				cats=line.split(',')
				user_nondepressed.append(cats[0])
			i=i+1
		print str(i)+": Non-Depressed Users"
		f.close()
		user_nondepressed_set = set(user_nondepressed)
		del user_nondepressed
		output_map = {}
		for k,v in user_cesd_map.iteritems():
			training_test_other = -1 # 0 for training, 1 for test, -1 for other
			depressed_nondepressed_other = -1 # 0 for depressed, 1 for nondepressed, -1 for other
			if k in user_train_set:
				training_test_other = 0
			elif k in user_test_set:
				training_test_other = 1
			else:
				training_test_other = -1
			if k in user_depressed_set:
				depressed_nondepressed_other = 0
			elif k in user_nondepressed_set:
				depressed_nondepressed_other = 1
			else:
				depressed_nondepressed_other = -1
			output_map[k]=(training_test_other,depressed_nondepressed_other,v)
		with open(pickle_file_LOC+'usermap.pkl','wb') as f:
			cPickle.dump(output_map,f)
	else:
		print "usermap.pkl Already Exists..."
		with open(pickle_file_LOC+'usermap.pkl','rb') as f:
			output_map=cPickle.load(f)
	return output_map
	
def get_vector_lists(userid,word_map):
	filename=userid+'.txt'
	filename2=userid+'.out'
	if not os.path.isfile(text_data_LOC+filename2):
		cm = twitter_LOC+'''twokenize.sh '''+text_data_LOC+filename+''' > '''+text_data_LOC+filename2
		process = subprocess.Popen(cm, shell=True, stdout=subprocess.PIPE)
		process.wait()
		mini_print(process.returncode)
	vector_lists=[]
	f=open(text_data_LOC+filename2,'rb')
	for line in f:
		word_list = line.strip().split(' ')
		vector_lists.append(convert_word_list(word_list,word_map))
	f.close()
	return vector_lists
	
def process_files(run_name,word_map,user_map):
	if not os.path.isfile(pickle_file_LOC+run_name+'.prepare.pkl'):
		print "Creating "+run_name+".prepare.pkl..."
		train_arr=[]
		test_arr=[]
		train_arr.append([])
		train_arr.append([])
		test_arr.append([])
		test_arr.append([])
		user_case_map={}
		train_i=0
		test_i=0
		user_data={}
		for k,v in user_map.iteritems():
			(train_test,class_n,cesd)=v
			if train_test >=0 and class_n >=0:
				vcs=get_vector_lists(k,word_map)
				indxs=[]
				if train_test==0: # training
					freqs = {}
					freqs_n = 0
					freqs_new = {}
					for vc in vcs:
						freqs_n = freqs_n+1
						if len(vc) >1: 
							for v in vc:
								if not v in freqs:
									freqs[v] = 1
								else:
									freqs[v] = 1+freqs[v]
							train_arr[class_n].append(vc)
							indxs.append(train_i)
							train_i=train_i+1
					for k_u,v_u in freqs.iteritems():
						freqs_new[k_u]=v_u/float(freqs_n)
				else: # testing
					freqs = {}
					freqs_n = 0
					freqs_new = {}
					for vc in vcs:
						freqs_n = freqs_n+1
						if len(vc) >1:
							for v in vc:
								if not v in freqs:
									freqs[v] = 1
								else:
									freqs[v] = 1+freqs[v]
							test_arr[class_n].append(vc)
							indxs.append(test_i)
							test_i=test_i+1
					for k_u,v_u in freqs.iteritems():
						freqs_new[k_u]=v_u/float(freqs_n)
				user_data[k] = freqs_new
				user_case_map[k]=(train_test,class_n,indxs)
		output = (train_arr,test_arr,user_case_map)
		with open(pickle_file_LOC+run_name+'.word2vec.pkl','wb') as f:
			cPickle.dump(user_data,f)
		with open(pickle_file_LOC+run_name+'.prepare.pkl','wb') as f:
			cPickle.dump(output,f)
	else:
		print run_name+".prepare.pkl Already Exists..."
		with open(pickle_file_LOC+run_name+'.prepare.pkl','rb') as f:
			output=cPickle.load(f)
	return output
	
def convert_files(run_name,output):
	if not os.path.isfile(pickle_file_LOC+run_name+'.run.pkl'):
		print "Converting Files into Theano Format..."
		(train_set_s,test_set_s,user_case_map)=output
		train_N = min(len(train_set_s[0]),len(train_set_s[1]))
		test_N = min(len(test_set_s[0]),len(test_set_s[1]))
		train_set = []
		train_set.append(np.concatenate((np.array(train_set_s[0][:train_N]),np.array(train_set_s[1][:train_N])),axis=0))
		train_set.append(np.concatenate((np.array(np.ones(train_N,dtype=np.int)),np.array(np.zeros(train_N,dtype=np.int))),axis=0))
		train_set=np.array(train_set)
		test_set = []
		test_set.append(np.concatenate((np.array(test_set_s[0][:test_N]),np.array(test_set_s[1][:test_N])),axis=0))
		test_set.append(np.concatenate((np.array(np.ones(test_N,dtype=np.int)),np.array(np.zeros(test_N,dtype=np.int))),axis=0))
		test_set=np.array(test_set)
		with open(pickle_file_LOC+run_name+'.run.pkl','wb') as f:
			cPickle.dump(train_set,f)
			cPickle.dump(test_set,f)
	else:
		print "Theano Formatted File Already Exists..."
	
def create_word_classes(size,window,negative,classes):
	run_name = 's'+str(size)+'w'+str(window)+'n'+str(negative)+'c'+str(classes)+'_classes'
	flname=run_name+'.sorted.out'
	if not os.path.isfile(flname):
		print "Creating "+flname+"..."
		cm='''cd '''+text_data_LOC+''' && time '''+word2vec_LOC+'''word2vec -train '''+run_name+'''.tokenized.out -output '''+run_name+'''.out -cbow 1 -size '''+str(size)+''' -window '''+str(window)+''' -negative '''+str(negative)+''' -hs 0 -sample 1e-4 -threads 20 -iter 15 -classes '''+str(classes)+''' && sort '''+run_name+'''.out -k 2 -n > '''+run_name+'''.sorted.out && echo The word classes were saved to file '''+run_name+'''.sorted.out'''
		process = subprocess.Popen(cm, shell=True, stdout=subprocess.PIPE)
		process.wait()
		print process.returncode
	else:
		print flname+" Already Exists..."
	word_map=load_convert_table(run_name)
	return run_name,word_map
		
def run(size,window,negative,classes):
	run_name,word_map=create_word_classes(size,window,negative,classes)
	user_map=load_info()
	output=process_files(run_name,word_map,user_map)
	convert_files(run_name,output)
	(train_set,test_set,user_case_map)=output
	print "Finished Preparing Files..."
	print "Summary:"
	if avg_missing_n>0:
		print "Percent of words thrown out: "+str(avg_missing/avg_missing_n)
	print "Training:\t# depressed: "+str(len(train_set[0]))+"\t# nondepressed: "+str(len(train_set[1]))
	print "Testing:\t# depressed: "+str(len(test_set[0]))+"\t# nondepressed: "+str(len(test_set[1]))
	print "Please Run:"
	print "python lstm.py -d "+pickle_file_LOC+run_name+'.run.pkl -m 100'
	
parser = argparse.ArgumentParser()
parser.add_argument("-s","--size", required=True,type=int,help="The vector dimensionality for word2vec")
parser.add_argument("-w", "--window", required=True,type=int,help="The size of the context window")
parser.add_argument("-n", "--negative", required=True,type=int,help="The number of negative examples")
parser.add_argument("-c", "--classes", required=True,type=int,help="The number classes")
args = parser.parse_args()
run(args.size,args.window,args.negative,args.classes)
