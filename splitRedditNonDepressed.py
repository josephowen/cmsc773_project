currentLine = 0

with open("project_materials/reddit-for-project/reddit-all/reddit-all-log.train.txt", 'w') as train:
    with open("project_materials/reddit-for-project/reddit-all/reddit-all-log.test.txt", 'w') as test:
        with open("project_materials/reddit-for-project/reddit-all/reddit-all-log.txt") as f:
            for line in f:
                if len(line) > 0:
                    currentLine += 1
                    if currentLine % 4 == 0:
                        test.write(line)
                    else:
                        train.write(line)