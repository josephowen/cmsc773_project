import csv
import os
import pdb

# Get user IDs from CSV
def getIds(filename):
    ids = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        header = reader.next()
        for row in reader:
            ids.append(row[0])
    return ids

if __name__ == "__main__":
    # Get user IDs for training and test sets
    train = getIds('users.train.csv')
    test = getIds('users.test.csv')
    
    # Get path to statuses
    path = os.path.join('.', 'project_materials', 'mypersonality_depression', 'text')
    
    # Get number of statuses
    numlines = 0
    for id in train:
        with open(os.path.join(path, id + '.txt'), 'r') as f:
            for i, l in enumerate(f):
                pass
        numlines += (i + 1)
    print "Training statuses: " + str(numlines)
    
    numlines = 0
    for id in test:
        with open(os.path.join(path, id + '.txt'), 'r') as f:
            for i, l in enumerate(f):
                pass
        numlines += (i + 1)
    print "Testing statuses: " + str(numlines)
