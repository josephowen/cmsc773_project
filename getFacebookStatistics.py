import string
import os
import sys
import csv

usersFilename = "./users.train.csv"

users_positive = []
users_negative = []

with open(usersFilename, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
            if row[-1] == "1":
                users_positive.append(row[0])
            else:
                users_negative.append(row[0])
        else:
            first = False

if len(sys.argv) > 1:
    fileBase = sys.argv[1]
    dot = fileBase.rfind(".")

    outFile = open(fileBase[:dot]+fileBase[dot:], 'w')
else:
    outFile = sys.stdout

emotes=[":)", "(:", ":(", "):",
        "=)", "(=", "=(", ")=",
        ":-)", "(-:", ":-(", ")-:",
        ";)", "(;", ";(", ");",
        ";-)", "(-;", ";-(", ")-;",
        ":'(", ")':", ":'c",
        ">:(", "):<", ">:c",
        ">=(", ")=<", ">=c",
        ":d", "d:", "=d", "d=",
        ":p", "p:", ";p", "p;",
        "o_o", "o.o",
        "^_^", "^.^", "^^",
        ">.<", ">_<", "><",
        ">.>", "<.<", ">_>", "<_<",
        ":3", "=3",
        "-.-", "-_-",
        ":]", "[:", ":[", "]:",
        "=]", "[=", "=[", "]=",
        ":|", "|:", "=|", "|=",
        "<3"]

def getDocWords(file):
    doc = []
    with open(file) as f:
        for line in f:
            thisLine = []
            words = line.split()
            for word in words:
                if not word in string.punctuation:
                    word = word.lower()

                    #thisLine.append(word.lower())
                    
                    if word in emotes:
                        thisLine.append(word)
                    else:
                        for char in string.punctuation:
                            word = word.strip(char)
                        if len(word) != 0:
                            thisLine.append(word)
                    
            doc.append(len(thisLine))
    return doc

positive = []
negative = []

dir = "./project_materials/mypersonality_depression/text/"

for i, user in enumerate(users_negative):
    negative += getDocWords(dir+user+".txt")
for i, user in enumerate(users_positive):
    positive += getDocWords(dir+user+".txt")

negativeUsers = len(users_negative)
positiveUsers = len(users_positive)

negativeTotal = sum(negative)
positiveTotal = sum(positive)

negativeCount = len(negative)
positiveCount = len(positive)

negativeAvg = negativeTotal/float(negativeCount)
positiveAvg = positiveTotal/float(positiveCount)

outFile.write("POSITIVE:\n")
outFile.write(str(positiveUsers) + " users\n")
outFile.write(str(positiveTotal) + " words\n")
outFile.write(str(positiveCount) + " statuses\n")
outFile.write(str(positiveAvg) + " words per status\n")
outFile.write("\n\nNEGATIVE\n")
outFile.write(str(negativeUsers) + " users\n")
outFile.write(str(negativeTotal) + " words\n")
outFile.write(str(negativeCount) + " statuses\n")
outFile.write(str(negativeAvg) + " words per status\n")

outFile.close()