# get average topic prob for each topic in each user's set of statuses, then average topic prob can be a feature
from operator import add
import os
import numpy as np
import matplotlib.pyplot as plt
from nltk.probability import FreqDist
import csv
import json

def average_topic_probs(user):
	#print 'opening ' + './topicDistributions/' + doc_id 
	data = [line.strip() for line in open(os.path.join('.', 'depression', 'topicDistributions', user + '.dat'), 'r')][3:]
	num_segs = len(data)
	totals = np.array([0] * 200)
	for dist in data:
		nums = np.array(map(float, dist.split(', ')))
		totals = totals + nums
	totals = totals/num_segs
	return totals

def get_num(x):
    return int(''.join(ele for ele in x if ele.isdigit()))

# write: map userid to [features, class] as JSON
def writeTopicAssignments(user, userClass, outFile_freq, outFile_prob):
	outFile_freq.write(user + ', ')
	outFile_prob.write(user + ', ')
	#userMap = {}
	#userMap['id'] = user
	#userMap['class'] = userClass
	topicMap = {}
	#featureMap = {}
	probs = average_topic_probs(user)

	for  i in range(200):
		outFile_prob.write(str(probs[i]) + ', ')
	for i in range(200):
		#outFile_prob.write(str(probs[i]) + ', ')
		topicMap[i] = 0
	data = [line.strip().split(', ') for line in open(os.path.join('.', 'depression', 'topicAssignment', user + '.dat'), 'r') if line.strip() != '']
	for seg in data:
		for assignment in seg:
			topic = get_num(assignment)
			topicMap[topic] += 1

	f = FreqDist(topicMap)
	for i in range(200):
		#featureMap['topic'+ str(i)] = {'count': topicMap[i], 'prob': probs[i]}
		outFile_freq.write(str(f.freq(i)) + ', ')
	#userMap['features'] = featureMap
	#json.dump(userMap, outFile)
	#outFile.write(',\n')
	
	outFile_freq.write(str(userClass) + '\n')
	outFile_prob.write(str(userClass) + '\n')
	#outFile_freq.write('\n')
	#outFile_prob.write('\n')
	
	return FreqDist(topicMap)

usersFilename_train = os.path.join('.', 'users.train.csv')
usersFilename_test = os.path.join('.', 'users.test.csv')
outFile_freqtrain = open(os.path.join('..', 'ldafreq.train.csv'), 'w')
outFile_freqtest = open(os.path.join('..', 'ldafreq.test.csv'), 'w')
outFile_probtrain = open(os.path.join('..', 'ldaprob.train.csv'), 'w')
outFile_probtest = open(os.path.join('..', 'ldaprob.test.csv'), 'w')

#outFile.write('[')
header_freq = 'userid'
header_prob = 'userid'
for i in range(200):
	header_freq += ', topic' + str(i) + 'freq'
	header_prob += ', topic' + str(i) + 'prob'
header_freq += ', class'
header_prob += ', class'
outFile_freqtrain.write(header_freq + '\n')
outFile_freqtest.write(header_freq + '\n')
outFile_probtrain.write(header_prob + '\n')
outFile_probtest.write(header_prob + '\n')

users_train = []
users_test = []
user_map = {}

with open(usersFilename_train, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
        	user_map[row[0]] = row[-1]
        	users_train.append(row[0])
        else:
            first = False

with open(usersFilename_test, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
            user_map[row[0]] = row[-1]
            users_test.append(row[0])
        else:
            first = False

fdists_negative = []
fdists_positive = []
outfileAssignments = open('topicmodel_assignments.csv', 'w')
outfileAssignments.write('topic,freq,color\n')
for i, user in enumerate(users_train):
	if os.path.isfile(os.path.join('.', 'depression', 'topicDistributions', user + '.dat')):
		print user
		if user_map[user] == 0:
			fdists_negative.append(writeTopicAssignments(user, 0, outFile_freqtrain, outFile_probtrain))
		else:
			fdists_positive.append(writeTopicAssignments(user, 1, outFile_freqtrain, outFile_probtrain))
		
for i, user in enumerate(users_test):
	if os.path.isfile(os.path.join('.', 'depression', 'topicDistributions', user + '.dat')):
		print user
		if user_map[user] == 0:
			fdists_negative.append(writeTopicAssignments(user, 0, outFile_freqtest, outFile_probtest))
		else:
			fdists_positive.append(writeTopicAssignments(user, 1, outFile_freqtest, outFile_probtest))

#outFile.write(']')
#outFile.close()
outFile_freqtrain.close()
outFile_freqtest.close()
outFile_probtrain.close()
outFile_probtest.close()

for dist in fdists_negative:
	for topic in dist.keys():
			freq = dist.freq(topic)
			outfileAssignments.write(str(topic) + ',' + str(freq) + ',red\n')
		#assignments_negative.append((int(topic), freq))
for dist in fdists_positive:
	for topic in dist.keys():
			freq = dist.freq(topic)
			outfileAssignments.write(str(topic) + ',' + str(freq) + ',cyan\n')
		#assignments_positive.append((int(topic), freq))

outfileAssignments.close()

# depressed_array = np.array(depressed_topicAssignments)
# depressed_t = depressed_array.T
# depressed_x, depressed_y = depressed_t
# undepressed_array = np.array(undepressed_topicAssignments)
# undepressed_t = undepressed_array.T
# undepressed_x, undepressed_y = undepressed_t


# fig = plt.figure()
# plt.scatter(depressed_x, depressed_y, c='b')
# plt.scatter(undepressed_x, undepressed_y, c='r')
# fig.suptitle('Topic frequency')
# plt.xlabel('Topic ID')
# plt.ylabel('Frequency of topic within documents')
# fig.savefig('topics.jpg')


# plt.show()

