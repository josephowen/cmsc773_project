package states;

import java.io.*;

import util.Matrix;
import util.Vector;

import data.*;

/**
 * 
 * @author Du Lan
 */
public class StatsTables implements Serializable {
	// Serialization
	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;

	private final Corpus corpus;
	private final int numTopics;
	private final int numTypes;
	// model level stats
	public int[][] MKW;
	public int[] MK;
	// table counts
	public int[][][] TIJK;
	public int[][] TIJ;
	// customer count
	public int[][][] NTIJK;
	public int[][][] NIJK;
	public int[][] NTIJ;

	/**
	 * 
	 * @param numTopics
	 *            the number of topics
	 * @param numTypes
	 *            number of word types
	 * @param corpus
	 *            corpus
	 */
	public StatsTables(int numTopics, int numTypes, Corpus corpus) {
		this.corpus = corpus;
		this.numTopics = numTopics;
		this.numTypes = numTypes;
		MKW = new int[numTopics][numTypes];
		MK = new int[numTopics];
		TIJ = new int[corpus.numDocs()][];
		TIJK = new int[corpus.numDocs()][][];

		NTIJ = new int[corpus.numDocs()][];
		NTIJK = new int[corpus.numDocs()][][];
		NIJK = new int[corpus.numDocs()][][];
		for (int i = 0; i < corpus.numDocs(); i++) {
			Document doc = corpus.getDoc(i);
			TIJ[i] = new int[doc.numTPs()];
			TIJK[i] = new int[doc.numTPs()][numTopics];

			NTIJ[i] = new int[doc.numTPs()];
			NTIJK[i] = new int[doc.numTPs()][numTopics];
			NIJK[i] = new int[doc.numTPs()][numTopics];
		}
	}

	/**
	 * Reset all the vectors and matrices.
	 */
	public void reset() {
		Matrix.fill(MKW, 0);
		Vector.fill(MK, 0);
		Matrix.fill(TIJ, 0);
		Matrix.fill(NTIJ, 0);
		for (int i = 0; i < corpus.numDocs(); i++) {
			Matrix.fill(NTIJK[i], 0);
			Matrix.fill(TIJK[i], 0);
			Matrix.fill(NIJK[i], 0);
		}
	}

	/**
	 * Adjust the table count by the given amount.
	 * 
	 * @param i
	 *            documnet index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic index
	 * @param val
	 *            amount to change
	 */
	public void adjustTable(final int i, final int j, final int k, final int val) {
		TIJK[i][j][k] += val;
		TIJ[i][j] += val;
		if (j > 0) {
			NTIJK[i][j - 1][k] += val;
			NTIJ[i][j - 1] += val;
		}
		if (ModelParams.debug) {
			assert TIJK[i][j][k] >= 0;
			assert TIJ[i][j] >= 0;
			if (j > 0) {
				assert NTIJK[i][j - 1][k] >= 0;
				assert NTIJ[i][j - 1] >= 0;
			}
		}
	}

	/**
	 * Adjust the customer (i.e., word) counts
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param w
	 *            word index
	 * @param k
	 *            topic index
	 * @param val
	 *            amount to change
	 */
	public void adjustCust(final int i, final int j, final int w, final int k,
			final int val) {
		MKW[k][w] += val;
		MK[k] += val;
		NIJK[i][j][k] += val;
		NTIJK[i][j][k] += val;
		NTIJ[i][j] += val;
		if (ModelParams.debug) {
			assert MKW[k][w] >= 0;
			assert MK[k] >= 0;
			assert NIJK[i][j][k] >= 0;
			assert NTIJK[i][j][k] >= NIJK[i][j][k];
			assert NTIJ[i][j] >= 0;
		}
	}

	/**
	 * Check the constraints on the recorded statistics.
	 */
	public void checkInvariance() {
		int sumMK = 0;
		int sumMKW;
		for (int k = 0; k < numTopics; k++) {
			sumMKW = 0;
			for (int w = 0; w < numTypes; w++) {
				assert MKW[k][w] >= 0 : "MKW[" + k + "][" + w + "] = "
						+ MKW[k][w];
				sumMKW += MKW[k][w];
			}
			assert MK[k] >= 0 : "MK[" + k + "] = " + MK[k];
			assert MK[k] == sumMKW : "MK[" + k + "] = " + MK[k] + ", Sum-MKW["
					+ k + "] = " + sumMKW;
			sumMK += MK[k];
		}
		assert sumMK == corpus.numWords() : "Sum-of-MK = " + sumMK
				+ ", corpus-total = " + corpus.numWords();

		int sumNTIJ = 0, sumTIJ = 0;
		for (int i = 0; i < corpus.numDocs(); i++) {
			Document doc = corpus.getDoc(i);
			assert doc.getDocIndex() == i;
			for (int j = 0; j < doc.numTPs(); j++) {
				TextPassage tp = doc.getTP(j);
				assert tp.getTPIndex() == j;
				int sumTIJK = 0, sumNTIJK = 0, sumNIJK = 0;
				for (int k = 0; k < numTopics; k++) {
					assert TIJK[i][j][k] >= 0;
					assert NIJK[i][j][k] >= 0;
					assert NTIJK[i][j][k] >= TIJK[i][j][k];
					assert NTIJK[i][j][k] >= NIJK[i][j][k];
					if (NTIJK[i][j][k] == 0)
						assert TIJK[i][j][k] == 0;
					if (TIJK[i][j][k] == 0)
						assert NTIJK[i][j][k] == 0;
					if (j < doc.numTPs() - 1)
						assert NTIJK[i][j][k] - TIJK[i][j + 1][k] == NIJK[i][j][k];
					else
						assert NTIJK[i][j][k] == NIJK[i][j][k];

					sumNIJK += NIJK[i][j][k];
					sumNTIJK += NTIJK[i][j][k];
					sumTIJK += TIJK[i][j][k];
				}
				assert sumNIJK == tp.size();
				assert TIJ[i][j] >= 0;
				assert TIJ[i][j] == sumTIJK;
				assert NTIJ[i][j] >= 0;
				assert sumNTIJK == NTIJ[i][j];
				if (j < doc.numTPs() - 1) {
					assert NTIJ[i][j] - TIJ[i][j + 1] == tp.size();
				} else {
					assert NTIJ[i][j] == tp.size();
				}
				if (j > 0)
					sumTIJ += TIJ[i][j];
				sumNTIJ += NTIJ[i][j];
			}
		}
		assert (sumNTIJ - sumTIJ) == corpus.numWords() : "sumNTIJ = " + sumNTIJ
				+ ", sumTIJ = " + sumTIJ + ", corpus.numWords = "
				+ corpus.numWords();
	}

	/**
	 * Save topic-by-word count matrix in format required by topic evaluation.
	 */
	public void saveTopicByWordCountMatrix(String file, Vocabulary voc) {
		try {
			FileWriter fw = new FileWriter(file);
			for (int k = 0; k < MKW.length; k++) {
				for (int w = 0; w < MKW[k].length; w++)
					fw.write(String.format("%d 1 t_%d %s\n", MKW[k][w],
							(k + 1), voc.getType(w)));
				fw.flush();
			}
			fw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Save statistics.
	 * 
	 * @param fileName
	 */
	public void writeObject(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			// write serial version
			out.writeInt(CURRENT_SERIAL_VERSION);
			// write MK and MKW
			for (int k = 0; k < numTopics; k++) {
				out.writeInt(MK[k]);
				for (int w = 0; w < numTypes; w++)
					out.writeInt(MKW[k][w]);
			}
			// write TIJ, NTIJ, NTIJK, TIJK
			for (int i = 0; i < corpus.numDocs(); i++) {
				for (int j = 0; j < corpus.getDoc(i).numTPs(); j++) {
					out.writeInt(TIJ[i][j]);
					out.writeInt(NTIJ[i][j]);
					for (int k = 0; k < numTopics; k++) {
						out.writeInt(NIJK[i][j][k]);
						out.writeInt(NTIJK[i][j][k]);
						out.writeInt(TIJK[i][j][k]);
					}
				}
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read StatsTables object from a file.
	 * 
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unused")
	public void readObject(String fileName) {
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fis);
			// read serial version
			int version = in.readInt();
			// write MK and MKW
			for (int k = 0; k < numTopics; k++) {
				MK[k] = in.readInt();
				for (int w = 0; w < numTypes; w++)
					MKW[k][w] = in.readInt();
			}
			// write TIJ, NTIJ, NTIJK, TIJK
			for (int i = 0; i < corpus.numDocs(); i++) {
				for (int j = 0; j < corpus.getDoc(i).numTPs(); j++) {
					TIJ[i][j] = in.readInt();
					NTIJ[i][j] = in.readInt();
					for (int k = 0; k < numTopics; k++) {
						NIJK[i][j][k] = in.readInt();
						NTIJK[i][j][k] = in.readInt();
						TIJK[i][j][k] = in.readInt();
					}
				}
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
