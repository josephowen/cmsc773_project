package states;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

import data.Corpus;

/**
 * 
 * @author Du Lan
 */
public class TopicDists implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;
	private final Corpus corpus;
	private double[][] docTopicDists;
	private double[][][] tpTopicDists;
	
	/**
	 * 
	 * @param c
	 * @param numTopics
	 */
	public TopicDists(Corpus c, int numTopics){
		this.corpus = c;
		docTopicDists = new double[c.numDocs()][numTopics];
		tpTopicDists = new double[c.numDocs()][][];
		for(int i = 0; i < c.numDocs(); i++)
			tpTopicDists[i] = new double[c.getDoc(i).numTPs()][numTopics];
	}
	
	/**
	 * Compute the document level and the passage level
	 * topic distributions
	 * 
	 * @param stables
	 * @param modelParams
	 */
	public void compute(StatsTables stables, ModelParams modelParams){
		for(int i = 0; i < stables.TIJ.length; i++)
		{//for each document
			for(int k = 0; k < modelParams.numTopics(); k++)
			{//for each topic
				docTopicDists[i][k] = (modelParams.getAlpha(k) + stables.TIJK[i][0][k]) 
										/ (modelParams.getAlphaSum() + stables.TIJ[i][0]);
				for(int j = 0; j < stables.TIJ[i].length; j++)
				{//for each text passage
					double val = stables.NTIJK[i][j][k] - modelParams.geta()*stables.TIJK[i][j][k];
					if(j == 0)
						val += (modelParams.geta()*stables.TIJ[i][j] + modelParams.getb(i)) 
													* docTopicDists[i][k];
					else
						val += (modelParams.geta()*stables.TIJ[i][j] + modelParams.getb(i))
													* tpTopicDists[i][j-1][k];
					val /= modelParams.getb(i) + stables.NTIJ[i][j];
					tpTopicDists[i][j][k] = val;
				}
			}
		}
	}

	/**
	 * Get the k-th topic probability for the i-th document
	 * 
	 * @param i
	 *            document index
	 * @param k
	 *            topic index
	 * @return
	 */
	public double getDocTopicDist(int i, int k){
		return docTopicDists[i][k];
	}
	
	/**
	 * Get the topic distribution for the i-th document
	 * 
	 * @param i
	 *            documen index
	 * @return
	 */
	public double[] getDocTopicDist(int i){
		return Arrays.copyOf(docTopicDists[i], docTopicDists[i].length);
	}
	
	/**
	 * Get the k-th topic probability for the j-th text passage of the i-th
	 * document
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic index
	 * @return
	 */
	public double getPassageTopicDist(int i, int j, int k){
		return tpTopicDists[i][j][k];
	}
	
	/**
	 * Get the topic distribution of the j-th text passage of the i-th document
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @return
	 */
	public double[] getPassageTopicDist(int i, int j){
		return Arrays.copyOf(tpTopicDists[i][j], tpTopicDists[i][j].length);
	}
	
	/**
	 * Save topic distributions for topic evolution analysis.
	 * @param oFileDir the root directory
	 */	
	public void writeTopicDistribution(String oFileDir){
		String folder = oFileDir + File.separator + "topicDistributions";
		(new File(folder)).mkdirs();
		try{
			FileWriter fw;
			for(int i = 0; i < docTopicDists.length; i++){
				fw = new FileWriter(folder+File.separator+corpus.getDoc(i).getDocName()+".dat");
				fw.write("doc-topic-distribution:\n");
				for(int k = 0; k < docTopicDists[i].length; k++){
					if(k == docTopicDists[i].length-1)
						fw.write(String.format("%.6f\n", docTopicDists[i][k]));
					else
						fw.write(String.format("%.6f, ", docTopicDists[i][k]));
				}
				fw.flush();
				fw.write("text-passage-topic-distributions (one per row):\n");
				for(int j = 0; j < tpTopicDists[i].length; j++){
					for(int k = 0; k < tpTopicDists[i][j].length; k++){
						if(k == tpTopicDists[i][j].length-1)
							fw.write(String.format("%.6f\n", tpTopicDists[i][j][k]));
						else
							fw.write(String.format("%.6f, ", tpTopicDists[i][j][k]));
					}
					fw.flush();
				}
				fw.close();
			}	
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		//write distribution in object format
		writeObject(oFileDir+File.separator+"final-topicDistribution.obj");
	}
	
	/**
	 * Save topic distributions in a specified file.
	 * 
	 * @param fileName
	 */
	public void writeObject(String fileName){
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			//write serial version
			out.writeInt(CURRENT_SERIAL_VERSION);
			//write distribution
			for(int i = 0; i < docTopicDists.length; i++){
				for(int k = 0; k < docTopicDists[i].length; k++)
					out.writeDouble(docTopicDists[i][k]);
				for(int j = 0; j < tpTopicDists[i].length; j++){
					for(int k = 0; k < tpTopicDists[i][j].length; k++)
						out.writeDouble(tpTopicDists[i][j][k]);
				}
			}
			out.flush();out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Read topic distributions from a specified file.
	 * 
	 * @param fileName
	 */
	@SuppressWarnings("unused")
	public void readObject(String fileName){
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fis);
			//read serial version
			int version = in.readInt();
			//read distributions
			for(int i = 0; i < docTopicDists.length; i++){
				for(int k = 0; k < docTopicDists[i].length; k++)
					docTopicDists[i][k] = in.readDouble();
				for(int j = 0; j < tpTopicDists[i].length; j++){
					for(int k = 0; k < tpTopicDists[i][j].length; k++)
						tpTopicDists[i][j][k] = in.readDouble();
				}
			}
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
