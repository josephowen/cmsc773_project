package states;

import util.Matrix;
import util.StirNum;

public final class StirRatioCache {
	private float[][] cacheOne;
	private float[][] cacheTwo;

	public StirRatioCache(boolean indicator) {
		cacheOne = new float[StirNum.maxM()][StirNum.maxN()];
		Matrix.fill(cacheOne, Float.NaN);
		if (indicator) {
			cacheTwo = new float[StirNum.maxM()][StirNum.maxN()];
			Matrix.fill(cacheTwo, Float.NaN);
			initCaches();
		} else {
			initCache();
		}
	}

	/**
	 * Initialise Stirling ratios for Inidcator sampler
	 */
	private void initCaches() {
		cacheOne[0][0] = (float) Math.exp(StirNum.logSN(1, 1) - StirNum.logSN(0, 0));
		for (int t = 1; t < cacheOne.length; t++) {
			for (int n = t; n < cacheOne[t].length; n++) {
				cacheOne[t][n] = (float) 
						(Math.exp(StirNum.logSN(n + 1, t + 1) - StirNum.logSN(n, t))
						* (t + 1.0) / (n + 1.0));
				cacheTwo[t][n] = (float) 
						(Math.exp(StirNum.logSN(n + 1, t) - StirNum.logSN(n, t))
						* (n - t + 1.0) / (n + 1.0));
			}
		}
	}

	/**
	 * Initial stirling ratios for Collapsed Sampler
	 */
	private void initCache() {
		cacheOne[0][0] = (float) Math.exp(StirNum.logSN(1, 1) - StirNum.logSN(0, 0));
		for (int t = 1; t < cacheOne.length; t++) {
			for (int n = t; n < cacheOne[t].length; n++)
				cacheOne[t][n] = (float) Math.exp(StirNum.logSN(n + 1, t) - StirNum.logSN(n, t));
		}
	}

	/**
	 * Get stirling ratio
	 * 
	 * @param n
	 * @param t
	 * @return
	 */
	public double getStirRatio(int n, int t) {
		if (t >= cacheOne.length || n >= cacheOne[0].length) {
			float[][] tmp = cacheOne;
			int sizeM = tmp.length;
			int sizeN = tmp[0].length;
			if (t > sizeM)
				sizeM += StirNum.EXPSIZE;
			if (n > sizeN)
				sizeN += StirNum.EXPSIZE;
			cacheOne = new float[sizeM][sizeN];
			Matrix.fill(cacheOne, Float.NaN);
			Matrix.copy(tmp, cacheOne);
			System.gc();
		}
		if (cacheOne[t][n] == Float.NaN)
			cacheOne[t][n] = (float) Math.exp(StirNum.logSN(n + 1, t) - StirNum.logSN(n, t));
		return cacheOne[t][n];
	}

	/**
	 * Get a cached value.
	 * 
	 * @param n
	 *            the number of customers
	 * @param t
	 *            the number of tables
	 * @return
	 */
	public double getCacheOne(final int n, final int t) {
		if (t >= cacheOne.length || n >= cacheOne[0].length) {
			float[][] tmp = cacheOne;
			int sizeM = tmp.length;
			int sizeN = tmp[0].length;
			if (t > sizeM)
				sizeM += StirNum.EXPSIZE;
			if (n > sizeN)
				sizeN += StirNum.EXPSIZE;
			cacheOne = new float[sizeM][sizeN];
			Matrix.fill(cacheOne, Float.NaN);
			Matrix.copy(tmp, cacheOne);
			System.gc();
		}
		if (cacheOne[t][n] == Float.NaN)
			cacheOne[t][n] = (float) (Math.exp(StirNum.logSN(n + 1, t + 1) - StirNum.logSN(n, t))
								* (t + 1.0) / (n + 1.0));
		return cacheOne[t][n];
	}

	/**
	 * Get a cached value.
	 * 
	 * @param n
	 * @param t
	 * @return
	 */
	public double getCacheTwo(final int n, final int t) {
		if (t >= cacheTwo.length || n >= cacheTwo[0].length) {
			float[][] tmp = cacheTwo;
			int sizeM = tmp.length;
			int sizeN = tmp[0].length;
			if (t > sizeM)
				sizeM += StirNum.EXPSIZE;
			if (n > sizeN)
				sizeN += StirNum.EXPSIZE;
			cacheTwo = new float[sizeM][sizeN];
			Matrix.fill(cacheTwo, Float.NaN);
			Matrix.copy(tmp, cacheTwo);
			System.gc();
		}
		if (cacheTwo[t][n] == Float.NaN)
			cacheTwo[t][n] = (float) (Math.exp(StirNum.logSN(n + 1, t) - StirNum.logSN(n, t))
									* (n - t + 1.0) / (n + 1.0));
		return cacheTwo[t][n];
	}
}
