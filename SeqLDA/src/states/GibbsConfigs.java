package states;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * 
 * @author Du Lan
 * 
 */
public final class GibbsConfigs {

	public long seed;
	public int numTopics;
	public double a;
	public double b;
	public double alpha;
	public double gamma;
	public boolean indicator;
	public boolean optB, slice, oneB;
	public boolean optAlpha, optGamma;
	public String corpusFile;
	public String segIndicator;
	public String trFileExt;
	public String teFileExt;
	public int trMaxIte, trBurnin, trLag;
	public int teMaxIte, teBurnin, teLag;
	public int numTopFreq, minDocFreq;
	public int maxN, maxM;
	public String phiFile;

	public GibbsConfigs(String configFile) {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration();
			config.load(configFile);
			corpusFile = config.getString("corpus");
			numTopFreq = config.getInt("word.topFreq");
			minDocFreq = config.getInt("word.minDocFreq");
			maxN = config.getInt("stirling.maxN");
			maxM = config.getInt("stirling.maxM");
			/*
			 * 
			 */
			if (config.containsKey("corpus.ind"))
				segIndicator = config.getString("corpus.ind");
			else
				segIndicator = null;
			/*
			 * Configure parameter optimization
			 */
			if (config.containsKey("opt.b")) {
				optB = config.getBoolean("opt.b");
				if (config.containsKey("opt.b.slice"))
					slice = config.getBoolean("opt.b.slice");
				else
					slice = false;
				if (config.containsKey("opt.b.corpus"))
					oneB = config.getBoolean("opt.b.corpus");
				else
					oneB = false;
			} else {
				optB = false;
				slice = false;
				oneB = false;
			}

			if (config.containsKey("opt.alpha"))
				optAlpha = config.getBoolean("opt.alpha");
			else
				optAlpha = false;

			if (config.containsKey("opt.gamma"))
				optGamma = config.getBoolean("opt.gamma");
			else
				optGamma = false;
			/*
			 * Configure the training Gibbs samplers
			 */
			trFileExt = config.getString("train.ext");
			trMaxIte = config.getInt("train.maxItes");
			trBurnin = config.getInt("train.burnIn");
			trLag = config.getInt("train.lag");
			/*
			 * Configure the testing Gibbs samplers
			 */
			if (config.containsKey("test.ext"))
				teFileExt = config.getString("test.ext");
			else
				teFileExt = null;
			if (teFileExt != null) {
				teMaxIte = config.getInt("test.maxItes");
				if (config.containsKey("test.burnIn"))
					teBurnin = config.getInt("test.burnIn");
				else
					teBurnin = 0;
				if (config.containsKey("test.lag"))
					teLag = config.getInt("test.lag");
				else
					teLag = 0;
			}

			if (config.containsKey("phiFile"))
				phiFile = config.getString("phiFile");
			else
				phiFile = null;
		} catch (ConfigurationException ce) {
			ce.printStackTrace();
		}
	}

	public void printConfiguration() {
		yap("Corpus = " + corpusFile);
		yap("SegIndicator = " + segIndicator);
		yap("Seed = " + seed);
		yap("NumTopics = " + numTopics);

		yap("alpha = " + alpha + ", gamma = " + gamma);
		yap("a = " + a + ", b = " + b);

		yap("Sampling Concetration parameters: ");
		yap("\t optb = " + optB + ", slice = " + slice);
		yap("Optimising alpha: " + optAlpha);
		yap("Optimising gamma: " + optGamma);

		yap("Stirling maxN = " + maxN + ", Stirling maxM = " + maxM);

		yap("Traning Gibbs iteration: file extension = " + trFileExt);
		yap("\t MaxItes = " + trMaxIte + ", BurnIn = " + trBurnin + ", Lag = "
				+ trLag);

		yap("Testing Gibbs iteration: file extension = " + teFileExt);
		yap("\t MaxItes = " + teMaxIte + ", BurnIn = " + teBurnin + ", Lag = "
				+ teLag);

		yap("Phi matrix: " + phiFile);

		yap("minDocFreq = " + minDocFreq + ", numTopFreq = " + numTopFreq);
	}

	private void yap(Object obj) {
		System.out.println(obj);
	}
}
