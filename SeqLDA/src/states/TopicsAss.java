package states;

import java.io.*;

import util.Vector;

import data.*;

/**
 * 
 * @author Du Lan
 */
public class TopicsAss implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;
	private Corpus corpus;
	private short[][][] topics;

	/**
	 * @param c
	 *            Corpus
	 */
	public TopicsAss(Corpus c) {
		this.corpus = c;
		topics = new short[c.numDocs()][][];
		for (int i = 0; i < c.numDocs(); i++) {
			Document doc = c.getDoc(i);
			topics[i] = new short[doc.numTPs()][];
			for (int j = 0; j < doc.numTPs(); j++)
				topics[i][j] = new short[doc.getTP(j).size()];
		}
	}

	/**
	 * Create topic assignment object with given topic assignments.
	 * 
	 * @param topics
	 *            prespecified topic assignments, a three-dimensional int array.
	 */
	public TopicsAss(short[][][] topics) {
		this.topics = new short[topics.length][][];
		for (int i = 0; i < topics.length; i++) {
			this.topics[i] = new short[topics[i].length][];
			for (int j = 0; j < topics[i].length; j++) {
				this.topics[i][j] = new short[topics[i][j].length];
				Vector.copy(topics[i][j], this.topics[i][j]);
			}
		}
	}

	/**
	 * 
	 * @return a 3-dimensional array.
	 */
	public short[][][] getAllTopics() {
		return topics;
	}

	/**
	 * @param i
	 *            the i-th document
	 * @param j
	 *            the j-th text passage in i
	 * @param n
	 *            the n-th word in j
	 * @param k
	 *            topic assignment
	 */
	public void setTopic(final int i, final int j, final int n, final int k) {
		topics[i][j][n] = (short)k;
	}

	/**
	 * 
	 * @param i
	 *            the i-th document
	 * @param j
	 *            the j-th text passage in i
	 * @param n
	 *            the n-th word in j
	 * @return
	 */
	public int getTopic(final int i, final int j, final int n) {
		return topics[i][j][n];
	}

	/**
	 * Save words and their topic assignments.
	 * 
	 * @param ofileDir
	 */
	public void writeWordAndTopic(final String ofileDir, Vocabulary voc) {
		FileWriter fw;
		String folder = ofileDir + File.separator + "topicAssignment";
		(new File(folder)).mkdirs();
		try {
			for (int i = 0; i < topics.length; i++) {
				fw = new FileWriter(folder + File.separator
						+ corpus.getDoc(i).getDocName() + ".dat");
				for (int j = 0; j < topics[i].length; j++) {
					for (int n = 0; n < topics[i][j].length; n++) {
						fw.write(String.format( "%s(%d)",
								voc.getType(corpus.getDoc(i).getTP(j)
										.getWord(n)), topics[i][j][n]));
						if (n < topics[i][j].length - 1)
							fw.write(", ");
					}
					fw.write("\n\n");
					fw.flush();
				}
				fw.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		// write topic assignment in object format
		writeObject(ofileDir + File.separator + "final-topicAss.obj");
	}

	/**
	 * Save topic assingments
	 * 
	 * @param fileName
	 */
	public void writeObject(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeInt(CURRENT_SERIAL_VERSION);
			for (int i = 0; i < topics.length; i++)
				for (int j = 0; j < topics[i].length; j++)
					for (int n = 0; n < topics[i][j].length; n++)
						oos.writeShort(topics[i][j][n]);
			oos.flush();
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load topic assignments from the specified file.
	 * 
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unused")
	public void readObject(String fileName) {
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			int version = ois.readInt();
			for (int i = 0; i < topics.length; i++)
				for (int j = 0; j < topics[i].length; j++)
					for (int n = 0; n < topics[i][j].length; n++)
						topics[i][j][n] = ois.readShort();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public static void main(String[] args){
//		short i = 10;
//		int j = (int) i;
//		System.out.println(String.format("i = %d, j = %d", i, j));
//		i = (short) j;
//		System.out.println(String.format("i = %d", i));
//	}
}
