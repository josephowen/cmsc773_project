package states;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

import data.Vocabulary;
import util.Vector;
import util.Matrix;
import util.MapUtil;

/**
 * 
 * @author Du Lan
 */
public class ModelParams implements Serializable {
	// Serialization
	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;
	private static final int NUMTOPWORDS = 100;
	// debugging
	public static int verboseLevel = 100;
	public static boolean debug = false;
	// folder to save trained model
	private String root;
	// Dirichlet parameters
	private double[] gammas, alphas;
	private double gammaSum, alphaSum;
	// The learnt topic distributions
	private double[][] phis;
	private boolean isPhiGiven = false;
	// Discount parameter
	private double a;
	// Concentration parameters
	private double[] bs;
	// the number of topics
	private int numTopics;
	// the vocabulary size
	private int numTypes;
	// number of documents
	private int numDocs;

	/**
	 * @param numTypes
	 *            the number of words in vocabulary
	 * @param numTopics
	 *            the number of topics
	 * @param numDocs
	 *            the total number of documents
	 * @param oFileDir
	 *            the output file directory
	 */
	public ModelParams(int numTypes, int numTopics, int numDocs) {
		this.numTypes = numTypes;
		this.numTopics = numTopics;
		this.numDocs = numDocs;
		alphas = new double[numTopics];
		gammas = new double[numTypes];
		bs = new double[numDocs];
		phis = new double[numTopics][numTypes];
	}

	/**
	 * Initialize the model parameters
	 * 
	 * @param a
	 *            the initial discount parameter
	 * @param b
	 *            the initial concentration parameter
	 * @param alpha
	 *            the initial Dirichlet parameter
	 * @param gamma
	 *            the initial Dirichlet parameter
	 */
	public void init(double a, double b, double alpha, double gamma) {
		this.a = a;
		Vector.fill(bs, b);

		Vector.fill(alphas, alpha);
		alphaSum = alpha * numTopics;

		Vector.fill(gammas, gamma);
		gammaSum = gamma * numTypes;

		isPhiGiven = false;
		Matrix.fill(phis, 0);
	}

	/**
	 * @param a
	 *            discount parameter
	 * @param b
	 *            concentration parameter
	 * @param alphas
	 *            Dirichlet parameter, a vector
	 * @param gammas
	 *            Dirichlet parameter, a vector
	 * @param phis
	 *            topic-by-word distribution matrix
	 */
	public void init(double a, double b, double[] alphas, double[] gammas,
			double[][] phis) {
		this.a = a;
		Vector.fill(bs, b);

		if (alphas.length != this.alphas.length)
			throw new RuntimeException("Illegal alpha parameters!!!");
		Vector.copy(alphas, this.alphas);
		alphaSum = Vector.sum(this.alphas);

		if (gammas.length != this.gammas.length)
			throw new RuntimeException("Illegal gamma parameters!!!");
		Vector.copy(gammas, this.gammas);
		gammaSum = Vector.sum(this.gammas);

		isPhiGiven = true;
		Matrix.copy(phis, this.phis);
	}

	/**
	 * 
	 * @param a
	 *            discount parameter
	 * @param b
	 *            concentration parameter
	 * @param alpha
	 *            Symmetric Dirichlet prior
	 * @param gamma
	 *            Symmetric Dirichlet prior
	 * @param phis
	 *            topic-by-word distribution matrix
	 */
	public void init(double a, double b, double alpha, double gamma,
			double[][] phis) {
		this.a = a;
		Vector.fill(bs, b);
		
		Vector.fill(alphas, alpha);
		alphaSum = alpha * numTopics;
		
		Vector.fill(gammas, gamma);
		gammaSum = gamma * numTypes;
		
		isPhiGiven = true;
		Matrix.copy(phis, this.phis);
	}

	/**
	 * Return the number of topics
	 * 
	 * @return
	 */
	public int numTopics() {
		return numTopics;
	}

	/**
	 * Return the number of word types in vocabulary.
	 * 
	 * @return
	 */
	public int numTypes() {
		return numTypes;
	}

	/**
	 * Set the output file directory.
	 * 
	 * @param file
	 */
	public void setRoot(String file) {
		root = file;
	}

	/**
	 * Return the output file directory.
	 * 
	 * @return
	 */
	public String root() {
		return root;
	}

	/**
	 * Return the discount parameter
	 * 
	 * @return
	 */
	public double geta() {
		return a;
	}

	/**
	 * return the concentration parameter for a document
	 * 
	 * @param i
	 *            document index
	 */
	public double getb(int i) {
		return bs[i];
	}

	/**
	 * Set the concentration parameter for a document
	 * 
	 * @param i
	 *            document index
	 * @param val
	 *            the value of the concentration parameter
	 */
	public void setb(int i, double val) {
		bs[i] = val;
	}

	/**
	 * Set the concentration parameters for all documents
	 * 
	 * @param val
	 */
	public void setb(double val) {
		Vector.fill(bs, val);
	}

	/**
	 * Return the mean value
	 * 
	 * @return
	 */
	public double meanb() {
		return Vector.mean(bs);
	}

	/**
	 * 
	 * @param k
	 * @return
	 */
	public double getAlpha(int k) {
		return alphas[k];
	}

	public double[] getAlpha() {
		return alphas;
	}

	public void setAlpha(int k, double val) {
		alphaSum += val - alphas[k];
		alphas[k] = val;
	}

	public void setAlpha(double val) {
		Vector.fill(alphas, val);
		alphaSum = val * numTopics;
	}

	public double getAlphaSum() {
		return alphaSum;
	}

	/**
	 * Dirichlet parameter gamma
	 */
	public double getGamma(int w) {
		return gammas[w];
	}

	public double[] getGamma() {
		return gammas;
	}

	public void setGamma(int w, double val) {
		gammaSum += val - gammas[w];
		gammas[w] = val;
	}

	public void setGamma(double val) {
		Vector.fill(gammas, val);
		gammaSum = val * numTypes;
	}

	public double getGammaSum() {
		return gammaSum;
	}

	/*
	 * topic-by-word matrix
	 */
	public double getPhi(int k, int w) {
		return phis[k][w];
	}

	public double[][] getPhi() {
		return phis;
	}

	public void setPhi(int k, int w, double val) {
		phis[k][w] = val;
	}

	public boolean isPhiGiven() {
		return isPhiGiven;
	}

	public void readPhiMatrix(String phiFile) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(phiFile));
			for (int k = 0; k < numTopics; k++) {
				String[] strs = br.readLine().split(", ");
				assert strs.length == numTypes;
				for (int w = 0; w < numTypes; w++)
					phis[k][w] = Double.parseDouble(strs[w]);
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		isPhiGiven = true;
	}

	/**
	 * Print the top words for each topic
	 * 
	 * @param fileName
	 *            file to save
	 * @param voc
	 *            vocabulary
	 */
	public void saveTopicByWords(String fileName, Vocabulary voc) {
		try {
			FileWriter fwriter = new FileWriter(new File(fileName));
			Map<String, Double> treeMap = new TreeMap<String, Double>();
			for (int k = 0; k < phis.length; k++) {
				for (int w = 0; w < phis[0].length; w++)
					treeMap.put(voc.getType(w), new Double(phis[k][w]));
				treeMap = MapUtil.sortByValueDecending(treeMap);
				fwriter.write("Topic-" + k + ": ");
				int count = 0;
				for (String key : treeMap.keySet()) {
					if (count == NUMTOPWORDS - 1)
						fwriter.write(String.format("%s(%.3f)\n", key,
								treeMap.get(key)));
					else
						fwriter.write(String.format("%s(%.3f) ", key,
								treeMap.get(key)));
					count++;
					if (count == NUMTOPWORDS)
						break;
				}
				fwriter.write("\n");
				fwriter.flush();
				treeMap.clear();
			}
			fwriter.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Save parameters
	 * 
	 * @param folder
	 *            the directory to save the paramter information.
	 */
	public void writeParameters(String folder) {
		String str;
		str = folder + File.separator + "seqlda-final-phiMatrix.dat";
		Matrix.write(phis, str);
		str = folder + File.separator + "seqlda-final-concentrations.dat";
		Vector.write(bs, str);
		str = folder + File.separator + "seqlda-final-alphas.dat";
		Vector.write(alphas, str);
		str = folder + File.separator + "seqlda-final-gammas.dat";
		Vector.write(gammas, str);
		// save in object format
		str = folder + File.separator + "seqkda-final-modelParams.obj";
		writeObject(str);
	}

	/**
	 * Save parameters in Obj format
	 * 
	 * @param fileName
	 */
	public void writeObject(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			// write serial version
			out.writeInt(CURRENT_SERIAL_VERSION);
			// write parameters
			out.writeInt(numTopics);
			out.writeInt(numTypes);
			out.writeInt(numDocs);
			out.writeDouble(alphaSum);
			out.writeDouble(gammaSum);
			out.writeDouble(a);
			// write concetration parameters
			for (int i = 0; i < bs.length; i++)
				out.writeDouble(bs[i]);
			// write alphas
			for (int i = 0; i < alphas.length; i++)
				out.writeDouble(alphas[i]);
			// write gammas
			for (int i = 0; i < gammas.length; i++)
				out.writeDouble(gammas[i]);
			// write phis
			for (int i = 0; i < phis.length; i++)
				for (int j = 0; j < phis[i].length; j++)
					out.writeDouble(phis[i][j]);

			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	public void readObject(String fileName) {
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fis);
			// read serial version
			int version = in.readInt();
			// read parameters
			numTopics = in.readInt();
			numTypes = in.readInt();
			numDocs = in.readInt();
			alphaSum = in.readDouble();
			gammaSum = in.readDouble();
			a = in.readDouble();
			// write concetration parameters
			bs = new double[numDocs];
			for (int i = 0; i < bs.length; i++)
				bs[i] = in.readDouble();
			// read alphas
			alphas = new double[numTopics];
			for (int i = 0; i < alphas.length; i++)
				alphas[i] = in.readDouble();
			// read gammas
			gammas = new double[numTypes];
			for (int i = 0; i < gammas.length; i++)
				gammas[i] = in.readDouble();
			// read phis
			phis = new double[numTopics][numTypes];
			for (int i = 0; i < phis.length; i++)
				for (int j = 0; j < phis[i].length; j++)
					phis[i][j] = in.readDouble();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
