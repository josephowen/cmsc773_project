import java.io.*;

import model.ModelEstimator;

import org.apache.commons.cli.*;

import data.Corpus;
import data.Vocabulary;

import states.GibbsConfigs;
import states.ModelParams;
import states.StirRatioCache;
import util.*;

/**
 * 
 * @author Du Lan
 *
 */
public class SeqLDA 
{
	private String root;
	private GibbsConfigs configs;
	private StirRatioCache cache;
	
	public SeqLDA(GibbsConfigs configs, String root)
	{
		this.configs = configs;
		this.root = root
					+ File.separator 
					+ String.format("k%03d-a%.2f-b%.2f-alpha%.2f-gamma%.2f", 
							configs.numTopics, configs.a, configs.b, 
							configs.alpha, configs.gamma);
		File rootFile = new File(root);
		if(!rootFile.exists())
			rootFile.mkdirs();
		/*
		 * make Stirling table
		 */
		StirNum.initialize(configs.maxN, configs.maxM, configs.a);
		cache = new StirRatioCache(configs.indicator);
	}
	
	/**
	 * Read topic-by-word matrix, which shoule have 
	 * each row contains probablity vector, the elmenets
	 * of which are seperated by ", ".
	 * @param numTypes the vocabulary size.
	 * @param phiFile the file storing the matrix.
	 */
	
	/**
	 * Run SeqLDA model 
	 * @param numGibbsRuns the number of experiments to be done
	 * @throws Exception
	 */
	public void run(int numGibbsRuns) throws Exception {
		String[] exts = new String[2];
		exts[0] = configs.trFileExt;
		exts[1] = configs.teFileExt;
		Vocabulary voc = new Vocabulary(configs.corpusFile, null, exts, 
										configs.minDocFreq, configs.numTopFreq);
		Corpus trCorpus = new Corpus(configs.corpusFile, voc, 
									 configs.trFileExt, configs.segIndicator);	
		Corpus teCorpus = null;
		if(configs.teFileExt != null)
			teCorpus = new Corpus(configs.corpusFile, voc, configs.teFileExt, 
								  configs.segIndicator);
		
		if(ModelParams.verboseLevel >= 1000){
			System.out.println("=======================================");
			System.out.println("voc size = "+voc.size());
			System.out.println("training dataset size = " + trCorpus.numDocs());
			if(teCorpus != null)
				System.out.println("testing dataset size = " + teCorpus.numDocs());
			System.out.println("=======================================");
			System.out.println("Training corpus:");
			trCorpus.printCorpusStats();
			System.out.println("Testing corpus:");
			teCorpus.printCorpusStats();
		}

		ModelEstimator estimator = new ModelEstimator(trCorpus, teCorpus, configs, voc, cache);
		estimator.estimate(numGibbsRuns, root);
	}
	
	/**
	 * Build command line options.
	 * @return
	 */
	public static Options buildOption(){
		Options options = new Options();
		options.addOption(new Option("h", "print the help message"));
		options.addOption(new Option("root", true, "output file directory"));
		@SuppressWarnings("static-access")
		Option config = OptionBuilder.withArgName("file")
									 .hasArg()
									 .withDescription("the model configuration file")
									 .create("config");
		options.addOption(config);
		options.addOption(new Option("i", false, "run indicator sampler"));
		options.addOption(new Option("k", true, "the number of topics"));
		options.addOption(new Option("alpha", true, "Dirichlet parameter alpha"));
		options.addOption(new Option("gamma", true, "Dirichlet parameter gamma"));
		options.addOption(new Option("a", true, "discount parameter of PYP"));
		options.addOption(new Option("b", true, "concetration parameter of PYP"));
		options.addOption(new Option("gnum", true, "the number of Gibbs runs"));
		options.addOption(new Option("debug", false, "turn on debugging"));
		options.addOption(new Option("verbose", true, "the verbose level"));
		return options;
	}
	
	public static void main(String[] args) throws Exception
	{
		Options options = buildOption();
		CommandLineParser parser = new GnuParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine line = null;
		try{
			line = parser.parse(options, args);
		}catch( ParseException exp ) {
		    System.err.println( "Unexpected exception:" + exp.getMessage() );
		    formatter.printHelp("SeqLDA", options);
			System.exit(1);
		}
		/*
		 * Print out help message
		 */
		if (line.hasOption("h")) {
			formatter.printHelp("SeqLDA", options);
			System.exit(0);
		}	
		/*
		 * Get the folder where all the outputs will be stored.
		 */
		String root = null;
		if(line.hasOption("root")){
			root = line.getOptionValue("root");		
		}else{
			System.err.println("Please specify the root directory \'-root\'");
			System.exit(1);
		}
		/*
		 * Get configuration file.
		 */
		String configFile = null;
		if(line.hasOption("config"))
			configFile = line.getOptionValue("config");		
		else{
			System.err.println("Please specify the configuration with \'-config\'");
			System.exit(1);
		}
		GibbsConfigs configs = new GibbsConfigs(configFile);
		/*
		 * Indicator sampler 
		 */
		if(line.hasOption("i"))
			configs.indicator = true;
		else
			configs.indicator = false;
		/*
		 * Get the number of topics for training and testing
		 */
		if(line.hasOption("k"))
			configs.numTopics = Integer.parseInt(line.getOptionValue("k"));
		else
			configs.numTopics = 50;
		
		if(line.hasOption("a"))
			configs.a = Double.parseDouble(line.getOptionValue("a"));
		else
			configs.a = 0.2;
		
		if(line.hasOption("b"))
			configs.b = Double.parseDouble(line.getOptionValue("b"));
		else
			configs.b = 10.0;
		
		if(line.hasOption("alpha"))
			configs.alpha = Double.parseDouble(line.getOptionValue("alpha"));
		else
			configs.alpha = 0.1;
		
		if(line.hasOption("gamma"))
			configs.gamma = Double.parseDouble(line.getOptionValue("gamma"));
		else
			configs.gamma = 0.01;
		
		/*
		 * the number of gibbs runs, the defualt value is one.
		 */
		int numGibbsRuns = 1;
		if(line.hasOption("gnum"))
			numGibbsRuns = Integer.parseInt(line.getOptionValue("gnum"));
		/*
		 * Run with debugging.
		 */
		if(line.hasOption("debug"))
			ModelParams.debug = true;
		/*
		 * Verbose level
		 */
		if(line.hasOption("verbose"))
			ModelParams.verboseLevel = Integer.parseInt(line.getOptionValue("verbose"));
		/*
		 * Start building the model
		 */
		SeqLDA seqlda = new SeqLDA(configs, root);
		seqlda.run(numGibbsRuns);
	}
}
