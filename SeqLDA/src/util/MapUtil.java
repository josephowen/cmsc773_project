package util;

import java.util.*;

public class MapUtil
{
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValueDecending(
			Map<K, V> map)
	{
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
			{
				int compare = (o1.getValue()).compareTo(o2.getValue());
				return -compare;
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValueAcending(
			Map<K, V> map)
	{
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
			{
				int compare = (o1.getValue()).compareTo(o2.getValue());
				return compare;
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
//	public static void main(String[] args){
//		Map<Integer, Double> testMap = new TreeMap<Integer, Double>();
//		testMap.put(1, 0.95);
//		testMap.put(2, 0.15);
//		testMap.put(3, 0.15);
//		testMap.put(4, 0.55);
//		testMap.put(5, 0.25);
//		testMap.put(6, 0.75);
//		System.out.println("UnsortedMap: "+testMap.toString());
//		testMap = MapUtil.sortByValueDecending(testMap);
//		System.out.println("SortedMap:"+testMap.toString());
//		for(Integer key : testMap.keySet())
//			System.out.println(key+":"+testMap.get(key));
//	}
}
