package model;

import gnu.trove.TIntArrayList;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import data.*;
import states.*;
import util.MTRandom;

public class ModelEstimator {
	// Vocabulary
	private final Vocabulary voc;
	// parameters for training
	private final TopicSampler trSampler;
	private final Corpus trCorpus;
	private final ModelParams trParams;
	private final GibbsConfigs configs;
	// corpus for computing perplexity
	private Corpus teCorpus = null;
	private ModelParams teParams;
	private TopicSampler teSampler;
	private Corpus ttrCorpus = null;
	private Corpus tteCorpus = null;
	//File writers
	private FileWriter llihoodWriter, perpWriter, bFileWriter;
	private FileWriter alphaWriter, gammaWriter;
	private FileWriter timeWriter;
	
	/**
	 * 
	 * @param trCorpus
	 *            training corpus
	 * @param teCorpus
	 *            testing corpus
	 * @param configs
	 *            gibbs configuration
	 * @param voc
	 *            vocabulary
	 */
	public ModelEstimator(Corpus trCorpus, Corpus teCorpus,
					      GibbsConfigs configs, Vocabulary voc,
					      StirRatioCache cache)
	{
		this.configs = configs;
		this.voc = voc;
		/*
		 * For training SeqLDA model
		 */
		this.trCorpus = trCorpus;
		this.trParams = new ModelParams(voc.size(), configs.numTopics, trCorpus.numDocs());
		if(configs.indicator)
			trSampler = new IndicatorSampler(trCorpus, trParams, cache);
		else
			trSampler = new CollapseSampler(trCorpus, trParams, cache);
		/*
		 * For testing SeqLDA model if teCorpus is given.
		 */
		if(teCorpus != null){
			this.teCorpus = teCorpus;
			splitTestCorpus(teCorpus);
			teParams = new ModelParams(voc.size(), configs.numTopics, ttrCorpus.numDocs());
			if(configs.indicator)
				teSampler = new IndicatorSampler(ttrCorpus, teParams, cache);
			else
				teSampler = new CollapseSampler(ttrCorpus, teParams, cache);
		}
	}
	
	/**
	 * 
	 * @param numGRuns
	 * @param root
	 * @throws Exception
	 */
	public void estimate(int numGRuns, String root) throws Exception{
		String oFileDir;
		double finalAvaPerplexity = 0;
		FileWriter fw = new FileWriter(root + "-perp.log");
		for(int n = 0; n < numGRuns; n++) {
			System.out.println("Gibbs run number: "+(n+1));
			//Set the random number generator seed
			configs.seed = System.currentTimeMillis();
			MTRandom.setSeed(configs.seed);
			//Set the output direcotry
			oFileDir = root + File.separator+"GibbsRun-" + (n+1);
			File file = new File(oFileDir);
			if(!file.exists()) file.mkdirs();
			//Initialise the parameters
			trParams.setRoot(oFileDir);
			trParams.init(configs.a, configs.b, configs.alpha, configs.gamma);
			if(configs.phiFile != null)
				trParams.readPhiMatrix(configs.phiFile);
			//Create a new model estimator
			double perp = run();
			finalAvaPerplexity += perp;
			//save perplexity
			fw.write(String.format("%.3f\n", perp));
			if(numGRuns > 1)
				trSampler.reset();
		}
		finalAvaPerplexity /= numGRuns;
		fw.write(String.format("final: %.3f\n", finalAvaPerplexity));
		fw.flush(); fw.close();
		System.out.println("\nFinal average perpelxity over "+numGRuns +" Gibbs runs: "+finalAvaPerplexity);
	}
	
	/**
	 * Run Gibbs sampler.
	 * 
	 * @return
	 */
	private double run() throws Exception {
		long totalTRTime = 0, iteRTime, iteSTime;
		double perplexity = 0.0;
		int numSamples = 0;
		//Start running the model
		initWriters();
		writeTrainingLogs(0, 0);
		long startTime = System.currentTimeMillis();
		for (int ite = 1; ite <= configs.trMaxIte; ite++) {
			iteSTime = System.currentTimeMillis();
			if(ite > 1)
				optHyperParams(trSampler, false);
			trSampler.runOneGibbsCycle();
			//Record running time and other stats
		    iteRTime = Math.round((System.currentTimeMillis() - iteSTime)/1000.0);
			totalTRTime += iteRTime;
			writeTrainingLogs(ite, iteRTime);
			//Draw samples
			if(ite > configs.trBurnin && ite%configs.trLag == 0){
				if(!trParams.isPhiGiven())
					trSampler.computePhis();
				if(teCorpus != null){
					perplexity += computePerplexity(numSamples+1);
				}
				numSamples++;
			}
		}
		/*
		 * Compute total running time
		 */
		long seconds = Math.round((System.currentTimeMillis() - startTime)/1000.0);
		long minutes = seconds / 60;	seconds %= 60;
		long hours = minutes / 60;	minutes %= 60;
		long days = hours / 24;	hours %= 24;
		System.out.print ("\nTotal running time: ");
		if (days != 0) { 
			System.out.print(days); 
			System.out.print(" days "); 
		}
		if (hours != 0) { 
			System.out.print(hours); 
			System.out.print(" hours "); 
		}
		if (minutes != 0) { 
			System.out.print(minutes); 
			System.out.print(" minutes "); 
		}
		System.out.print(seconds); 
		System.out.println(" seconds");
		timeWriter.write(String.format("Total running time: %d days, " +
				"%d hours, %d minutes, %d seconds\n\n", days, hours, minutes, seconds));
		
		/*
		 * Compute total training time and training time per Gibbs iteration
		 */
		seconds = totalTRTime;
		minutes = seconds / 60;	seconds %= 60;
		hours = minutes / 60;	minutes %= 60;
		days = hours / 24;	hours %= 24;
		System.out.print ("\nTotal training time: ");
		if (days != 0) { 
			System.out.print(days); 
			System.out.print(" days "); 
			}
		if (hours != 0) { 
			System.out.print(hours); 
			System.out.print(" hours "); 
			}
		if (minutes != 0) { 
			System.out.print(minutes); 
			System.out.print(" minutes "); 
			}
		System.out.print(seconds); 
		System.out.println(" seconds");
		System.out.println("Average training time per ite: " + totalTRTime/ configs.trMaxIte);
		
		timeWriter.write(String.format("Total training time: %d days, " +
				"%d hours, %d minutes, %d seconds\n", days, hours, minutes, seconds));
		timeWriter.write(String.format("Average training time per iteration: %.3f", 
				(double)totalTRTime/configs.trMaxIte));
		closeWriters();
		/*
		 * save model details
		 */
		trSampler.writeTRSamplingInfo(voc);
		/*
		 * Computer average perplexity
		 */
		if(teCorpus != null) {
			perplexity /= numSamples;
			System.out.println("\nAverage perplexity = "+perplexity);
		}
		return perplexity;
	}
	
	/**
	 * Return testing perplexity.
	 * 
	 * @param sampleIndex 
	 * @return
	 */
	private double computePerplexity(int sampleIndex)
	{
		double perp = 0.0;
		int nsamples = 0;
		String folder = trParams.root()+File.separator+"sample-"+sampleIndex;
		(new File(folder)).mkdirs();
		teParams.setRoot(folder);
		teParams.init(trParams.geta(), trParams.meanb(), 
					  trParams.getAlpha(), trParams.getGamma(), 
					  trParams.getPhi());
		for (int ite = 1; ite <= configs.teMaxIte; ite++) {
			if(ite > 1)
				optHyperParams(teSampler, true);
			teSampler.runOneGibbsCycle();
			if(ModelParams.verboseLevel >= 12000){
				String str = String.format("Perplexity testing: ite = %4d, " +
										   "loglikelihood = %.3f, trPerplexity = %.3f\n", 
										   ite, teSampler.logLikelihood(),
										   teSampler.perplexity());
				System.out.println(str);
			}
			if(configs.teBurnin > 0 && configs.teLag > 0){
				if(ite > configs.teBurnin && ite%configs.teLag == 0){
					perp += teSampler.perplexity(tteCorpus);
					nsamples ++;
				}
			}else if (ite == configs.teMaxIte){
				perp = teSampler.perplexity(tteCorpus);
				nsamples = 1;
			}
		}
		teSampler.writeTESamplingInfo(voc);
		perp /= nsamples;
		if(ModelParams.verboseLevel >= 10000)
			System.out.println("Draw sample number = "+sampleIndex+", perp = "+perp);
		teSampler.reset();
		System.gc();
		return  perp;
	}
	
	private void optHyperParams(TopicSampler sampler, boolean testing)
	{
		if(configs.optB)
			sampler.optimiseConcentration(configs.slice, configs.oneB);
		if(!testing){
			if(configs.optAlpha)
				sampler.optimiseSymmetricAlpha();
			if(configs.optGamma)
				sampler.optimiseSymmetricGamma();
		}
	}
	
	private void writeTrainingLogs(int ite, long iteRTime) throws Exception
	{
		double logLikelihood = trSampler.logLikelihood();
		llihoodWriter.write(String.format("%.3f\n", logLikelihood));
		llihoodWriter.flush();
		
		double trainPerp = trSampler.perplexity();
		perpWriter.write(String.format("%.3f\n", trainPerp));
		perpWriter.flush();
		
		if(configs.optB){
			if(configs.oneB) {
				bFileWriter.write(String.format("%.3f\n", trParams.getb(0)));
			} else {
				for(int i = 0; i < trCorpus.numDocs(); i++) {
					if(i < trCorpus.numDocs()-1)
						bFileWriter.write(String.format("%.3f, ", trParams.getb(i)));
					else
						bFileWriter.write(String.format("%.3f\n", trParams.getb(i)));
				}
			}
			bFileWriter.flush();
		}
		
		if(configs.optAlpha) {
			alphaWriter.write(String.format("%.2f\n", trParams.getAlpha(0)));
			alphaWriter.flush();
		}
		
		if(configs.optGamma) {
			gammaWriter.write(String.format("%.2f\n", trParams.getGamma(0)));
			gammaWriter.flush();
		}
		
		if(ModelParams.verboseLevel >= 3000)
			System.out.println(String.format("Training Ite: %5d, runTime: %3d seconds, " 
												+  "likelihood: %.3f, trainPerp: %.3f", 
												ite, iteRTime, logLikelihood, trainPerp));
	}
	
	/**
	 * Initialise all the file writers.
	 * 
	 * @throws Exception
	 */
	private void initWriters() throws Exception{
		llihoodWriter = new FileWriter(trParams.root() 
			  								+ File.separator
			  								+ "modelLogLikelihood.log");
		perpWriter = new FileWriter(trParams.root() 
										+ File.separator
										+ "traingPerplexity.log");
		if(configs.optB)
			bFileWriter = new FileWriter(trParams.root() 
											+ File.separator
											+ "trainedBs.log");
		if(configs.optAlpha)
			alphaWriter = new FileWriter(trParams.root() 
											+ File.separator
											+ "optimisedAlpha.log");
		if(configs.optGamma)
			gammaWriter = new FileWriter(trParams.root() 
					+ File.separator
					+ "optimisedGamma.log");
		
		timeWriter = new FileWriter(trParams.root()
									+ File.separator
									+ "runingTime.log");
	}
	
	/**
	 * Close all the file writers.
	 * 
	 * @throws Exception
	 */
	private void closeWriters() throws Exception{
		llihoodWriter.flush(); 
		llihoodWriter.close();
		perpWriter.flush(); 
		perpWriter.close();
		if(configs.optB){
			bFileWriter.flush(); 
			bFileWriter.close();
		}
		if(configs.optAlpha) {
			alphaWriter.flush(); 
			alphaWriter.close();
		}
		if(configs.optGamma) {
			gammaWriter.flush(); 
			gammaWriter.close();
		}
		timeWriter.flush();
		timeWriter.close();
	}
	
	/**
	 * Split the testing corpus into two parts:
	 * 	c1: for learning topic distributions.
	 *  c2: for computing perplexity.
	 * @param testing 
	 * 					testing corpus
	 */
	private void splitTestCorpus(Corpus testing){
		ArrayList<Document> doclist1 = new ArrayList<Document>();
		ArrayList<Document> doclist2 = new ArrayList<Document>();
		for(int i = 0; i < testing.numDocs(); i++){
			Document doc = testing.getDoc(i);
			assert doc.getDocIndex() == i;
			ArrayList<TextPassage> tplist1 = new ArrayList<TextPassage>();
			ArrayList<TextPassage> tplist2 = new ArrayList<TextPassage>();
			for(int j = 0; j < doc.numTPs(); j++){
				TextPassage tp = doc.getTP(j);
				assert tp.getTPIndex() == j;
				TIntArrayList wlist1 = new TIntArrayList(); 
				TIntArrayList wlist2 = new TIntArrayList();
				for(int n = 0; n < tp.size(); n++){
					if(n%2==0){
						wlist1.add(tp.getWord(n));
					}else{
						wlist2.add(tp.getWord(n));
					}
				}
				
				if(ModelParams.debug && ModelParams.verboseLevel >= 20000)
					if(wlist1.size() == 0 || wlist2.size() == 0)
						System.out.println("i = "+i+", j = "+j+", wlist1 = "
									+wlist1.size()+", wlist2 = "+wlist2.size());
				
				tplist1.add(new TextPassage(wlist1, j, null));
				tplist2.add(new TextPassage(wlist2, j, null));
			}
			assert tplist1.size() == tplist2.size();
			doclist1.add(new Document(tplist1, null, i, doc.getDocName()));
			doclist2.add(new Document(tplist2, null, i, doc.getDocName()));
		}
		ttrCorpus = new Corpus(doclist1);
		tteCorpus = new Corpus(doclist2);
		
		assert doclist1.size() == doclist2.size();
		assert ttrCorpus.numDocs() == tteCorpus.numDocs();
		
		if(ModelParams.debug){
			ttrCorpus.printCorpusStats();
			tteCorpus.printCorpusStats();
		}
	}
}
