package model;

import java.io.Serializable;
import states.ModelParams;
import states.StirRatioCache;
import util.MTRandom;
import util.SpecialFuns;
import util.StirNum;
import data.Corpus;

/**
 * 
 * @author Du Lan
 */
public class IndicatorSampler extends TopicSampler implements Serializable {
	//Serialization
	private static final long serialVersionUID = 1L;
	/**
	 * Table indicator sampler for SeqLDA.
	 * 
	 * @param corpus
	 * @param modelParams
	 */
	public IndicatorSampler (Corpus corpus, ModelParams modelParams, 
							 StirRatioCache cache)
	{
		super(corpus, modelParams, cache);
		if(ModelParams.debug)
			System.out.println("\n Run indicator sampling algorithm!!!\n");
	}
	
	/**
	 * 
	 */
	public void reset() {
		stables.reset();
		initialiseStates();
		System.gc();
	}
		
	/**
	 * Jointly sample topic and table indicator, and return the sample topic.
	 * 
	 * @return
	 */
	protected int sample(final int i, final int j, final int w, int k){
		int jj;
		double probk, val;
		if(!remove(i, j, w, k))
			return -1;
		for(k = 0; k < modelParams.numTopics(); k++) {
			probk = 0;
			if(modelParams.isPhiGiven())
				val = modelParams.getPhi(k, w);
			else
				val = (modelParams.getGamma(w) + stables.MKW[k][w])
						/(modelParams.getGammaSum() + stables.MK[k]);
			/*
			 * Consider not contributing a table.
			 */
			if(stables.TIJK[i][j][k] > 0)
				probk += this.probIndZero(i, j, k, val);
			/*
			 * Consider contributing a table
			 */
			for(jj = 0; jj <= j; jj++)
				if(jj == 0 || (jj > 0 && stables.TIJK[i][jj-1][k] > 0))
					probk += this.probIndOne(i, j, k, jj, val);
				else 
					break;
			topicProbs[k] = probk;
		}
		k = nextDiscrete(topicProbs);
		//add a customer
		this.add(i, j, w, k);
		return k;
	}
	
	/**
	 * Sample to add a customer
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param w
	 *            word index
	 * @param k
	 *            topic index
	 */
	private void add(final int i, final int j, final int w, final int k) {
		double phi, tmpk = 0;
		double randVal = MTRandom.nextDouble()*topicProbs[k];
		boolean tableInd = false;
		int passageInd = j;
		
		if(modelParams.isPhiGiven())
			phi = modelParams.getPhi(k, w);
		else
			phi = (modelParams.getGamma(w) + stables.MKW[k][w])
					/(modelParams.getGammaSum() + stables.MK[k]);
		
		if(stables.TIJK[i][j][k] > 0){
			tmpk += this.probIndZero(i, j, k, phi);
			if(tmpk > randVal) {
				add(i, j, w, k, passageInd, tableInd);
				return;
			}	
		}
		for(int jj = 0; jj <= j; jj++) {
			if(jj == 0 || (jj > 0 && stables.TIJK[i][jj-1][k] > 0)){
				tmpk += this.probIndOne(i, j, k, jj, phi);
				tableInd = true;
				passageInd = jj;
				if(tmpk > randVal) {
					add(i, j, w, k, passageInd, tableInd);
					return;
				}
			} else 
				break;
		}
		add(i, j, w, k, passageInd, tableInd);
	}
	
	private void add(final int i, final int j, final int w, final int k, 
					 final int add2j, boolean tableInd)
	{
		if(tableInd)
			for(int jj = add2j; jj <= j; jj++)
				stables.adjustTable(i, jj, k, 1);
		stables.adjustCust(i, j, w, k, 1);
	}
	
	/**
	 * Remove a cutomer.
	 * @param i
	 * @param j
	 * @param w
	 * @param k
	 * @return
	 */
	private boolean remove(final int i, final int j, final int w, final int k) {
		double probInd;
		int n, t;
		boolean tableInd = false; 
		int passageInd = j;
		for (int jj = j; jj >= 0; jj--) {
			n = stables.NTIJK[i][jj][k]; assert n > 0;
			t = stables.TIJK[i][jj][k]; assert t > 0;
			probInd = (double) t / n;
			if(probInd > MTRandom.nextDouble()){ //if contribute a table
				if (t == 1 && n > t)
					return false;
				else {
					tableInd = true;
					passageInd = jj;
				}
			}else{
				break;
			}
		}
		if (tableInd){
			for (int jj = passageInd; jj <= j; jj++)
				stables.adjustTable(i, jj, k, -1);
		}
		/*
		 * Remove customer
		 */
		stables.adjustCust(i, j, w, k, -1);
		return true;
	}
	
	/**
	 * Return the joint probablity of increasing both NIJK and TIJK by one.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            the current text passage index
	 * @param k
	 *            topic index
	 * @param add2j
	 *            up to which text passage a table is created.
	 * @return
	 */
	private double probIndOne(final int i, final int j, final int k,
							  final int add2j, double val){
		double a = modelParams.geta();
		double b = modelParams.getb(i);
		//Text passage: add2j - 1
		int jj;
		if(add2j > 0){
			jj = add2j - 1;
			val *= cache.getCacheTwo(stables.NTIJK[i][jj][k], stables.TIJK[i][jj][k])
							/(b + stables.NTIJ[i][jj]);
		}else{
			val *= (modelParams.getAlpha(k) + stables.TIJK[i][0][k])
					/(modelParams.getAlphaSum() + stables.TIJ[i][0]);
		}
		/*
		 * Text passage from add2j, both customer and table counts 
		 * increase by one.
		 */
		for(jj = add2j; jj <= j; jj++){
			val *= cache.getCacheOne(stables.NTIJK[i][jj][k], stables.TIJK[i][jj][k])
							*(b + a*stables.TIJ[i][jj])/(b + stables.NTIJ[i][jj]);
		}
		if(!SpecialFuns.isnormal(val) || val < 0)
			throw new RuntimeException("Illegal indicator probability (ind = 1)!!! val = "+val);
		return val;
	}

	
	/**
	 * Return the joint probability of increaing NIJK by one
	 * and keeping TIJK unchanged.
	 * @param i document index
	 * @param j text passage index
	 * @param k topic index
	 * @param val phi value
	 * @return
	 */
	private double probIndZero(final int i, final int j, final int k, double val){
		val *= cache.getCacheTwo(stables.NTIJK[i][j][k], stables.TIJK[i][j][k])
						/(modelParams.getb(i) + stables.NTIJ[i][j]);
		if(!SpecialFuns.isnormal(val) || val < 0)
			throw new RuntimeException("Illegal indicator probability (ind = 0)!!! val = "+val);
		return val;
	}
	
	/**
	 * Return the minus log likelihood of the model.
	 */
	public double logLikelihood() {
		double logLikelihood = globalLLL();
		for (int i = 0; i < corpus.numDocs(); i++) {
			logLikelihood += SpecialFuns.logGamma(modelParams.getAlphaSum())
								- SpecialFuns.logGamma(modelParams.getAlphaSum() + stables.TIJ[i][0]);
			for (int k = 0; k < modelParams.numTopics(); k++)
				logLikelihood += SpecialFuns.logGamma(modelParams.getAlpha(k) + stables.TIJK[i][0][k])
				 				 	- SpecialFuns.logGamma(modelParams.getAlpha(k));
			
			for (int j = 0; j < corpus.getDoc(i).numTPs(); j++) {	
				logLikelihood += SpecialFuns.logPochSym(modelParams.getb(i), modelParams.geta(), 
														stables.TIJ[i][j])
									- SpecialFuns.logPochSym(modelParams.getb(i), 1.0, stables.NTIJ[i][j]);
				for (int k = 0; k < modelParams.numTopics(); k++)
					logLikelihood += StirNum.logSN(stables.NTIJK[i][j][k], stables.TIJK[i][j][k])
									- SpecialFuns.logChoose(stables.NTIJK[i][j][k], stables.TIJK[i][j][k]);
			}
		}
		logLikelihood /= - corpus.numWords();
		if (!SpecialFuns.isnormal(logLikelihood)) 
			throw new RuntimeException("Table indicator sampler has illegal log likelihood.");
		return logLikelihood;
	}
	
	void yap(Object obj){
		System.out.println(obj);
	}
}
