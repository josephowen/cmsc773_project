package model;

import java.io.*;

import optimizer.*;
import data.*;
import states.*;
import util.Vector;
import util.MTRandom;
import util.SpecialFuns;

/**
 * 
 * @author Du Lan
 *
 */
public abstract class TopicSampler implements Serializable 
{
	//Serialization
	private static final long serialVersionUID = 1L;
	private static boolean doPolya = true;
	//Model parameters
	private TopicsAss ass;
	protected Corpus corpus;
	protected StatsTables stables;
	protected ModelParams modelParams;
	protected TopicDists topicDists;
	protected double[] topicProbs;
	//Caching stirling ratio values
	protected StirRatioCache cache;
	/**
	 * @param corpus
	 * @param modelParams
	 */
	public TopicSampler(Corpus corpus, ModelParams modelParams, StirRatioCache cache) {
		this.corpus = corpus;
		this.modelParams = modelParams;
		this.cache = cache;
		ass = new TopicsAss(corpus);
		stables = new StatsTables(modelParams.numTopics(), modelParams.numTypes(), corpus);
		topicDists = new TopicDists(corpus, modelParams.numTopics());
		topicProbs = new double[modelParams.numTopics()];
		initialiseStates();
	}
	
	/**
	 * Reset the sampler by random initilization.
	 */
	public abstract void reset(); 
	
	/**
	 * Return the sampled topic for a word.
	 * 
	 * @param i
	 *            documnet index
	 * @param j
	 *            text passage index
	 * @param w
	 *            word index
	 * @param k
	 *            old topic index
	 * @return
	 */
	protected abstract int sample(final int i, final int j, final int w, int k);
	
	/**
	 * Return the minus model log likelihood.
	 * 
	 * @return
	 */
	public abstract double logLikelihood();
	
	/**
	 * Run one Gibss sampling iteration through all the documents.
	 */
	public void runOneGibbsCycle() {
		int k;
		for (int i = 0; i < corpus.numDocs(); i++) {
			for (int j = 0; j < corpus.getDoc(i).numTPs(); j++) {
				TextPassage tp = corpus.getDoc(i).getTP(j);
				for (int n = 0; n < tp.size(); n++) {
					k = ass.getTopic(i, j, n);
					k = sample(i, j, tp.getWord(n), k);
					if (k > -1)
						ass.setTopic(i, j, n, k);
				}
			}
		}
		if(ModelParams.debug)
			stables.checkInvariance();
	}

	/**
	 * Sample a value from a double array;
	 * 
	 * @param probs
	 *            an double array
	 * @return
	 */
	protected int nextDiscrete(double[] probs) {
        double sum = 0.0;
        double r =  MTRandom.nextDouble() * Vector.sum(probs);
        for (int i = 0; i < probs.length; i++) {
            sum += probs[i];
            if (sum > r)  return i;
        }
        return probs.length - 1;
    }
	
	/**
	 * Randomly initialise the states. All the customers eating the same dish
	 * will sit at one table initially.
	 */
	protected void initialiseStates() {
		int k;
		Document doc;
		TextPassage tp;
		for (int i = 0; i < corpus.numDocs(); i++){
			doc = corpus.getDoc(i);
			for (int j = doc.numTPs()-1; j >= 0; j--) {
				tp = corpus.getDoc(i).getTP(j);
				//Initialise topic assigment and corresponding counts
				for (int n = 0; n < tp.size(); n++) {
					k = MTRandom.nextInt(modelParams.numTopics());
					ass.setTopic(i, j, n, k);
					stables.adjustCust(i, j, tp.getWord(n), k, 1);
				}
				/*
				 * Initialise table counts by assuming 
				 * all the customers eating the same dish sit 
				 * at the same table.
				 */
				for (k = 0; k < modelParams.numTopics(); k++) 
					if (stables.NTIJK[i][j][k] > 0) 
							stables.adjustTable(i, j, k, 1);
			}
		}
		if(ModelParams.debug)
			stables.checkInvariance();
	}
	
	/**
	 * Compute the topics-by-words matrix, which is the learnt topic
	 * distributions.
	 */
	public void computePhis() {
		double val;
		for (int k = 0; k < modelParams.numTopics(); k++) {
			for (int w = 0; w < modelParams.numTypes(); w++) {
				val = (modelParams.getGamma(w) + stables.MKW[k][w]) 
						/ (modelParams.getGammaSum() + stables.MK[k]);
				assert val > 0 : "phis["+k+"]["+w+"] = "+val;
				modelParams.setPhi(k, w, val);
			}
		}
	}
	
	/**
	 * Return the global log likelihood related to topic-by-word matrix.
	 * 
	 * @return
	 */
	protected double globalLLL(){
		double logLikelihood = 0;
		if(modelParams.isPhiGiven()){
			for (int k = 0; k < modelParams.numTopics(); k++) {
				logLikelihood += SpecialFuns.logGamma(modelParams.getGammaSum());
				for (int w = 0; w < modelParams.numTypes(); w++) 
					logLikelihood += (stables.MKW[k][w] + modelParams.getGamma(w) - 1)
										*Math.log(modelParams.getPhi(k, w))
									- SpecialFuns.logGamma(modelParams.getGamma(w));
			}
		}else{
			for (int k = 0; k < modelParams.numTopics(); k++) {
				logLikelihood += SpecialFuns.logGamma(modelParams.getGammaSum())
								 - SpecialFuns.logGamma(modelParams.getGammaSum() + stables.MK[k]);
				for (int w = 0; w < modelParams.numTypes(); w++) {
					logLikelihood += SpecialFuns.logGamma(modelParams.getGamma(w) + stables.MKW[k][w])
									 - SpecialFuns.logGamma(modelParams.getGamma(w));
				}
			}
		}
		return logLikelihood;
	}
	
	/**
	 * Optimize the concentration parameters
	 * 
	 * @param doSlice
	 *            boolean variable which indicates whether using slice sampler
	 *            or not.
	 * @param doOneB
	 *            if we optimise only corpus level concentration parameter.
	 */
	public void optimiseConcentration(boolean doSlice, boolean doOneB)
	{
		if(doSlice){
			SliceBSampler sliceB = new SliceBSampler(stables, modelParams.geta(), 
													 MTRandom.generator());
			if(doOneB){
				 modelParams.setb(sliceB.sample(modelParams.getb(0)));
			}else{
				for(int i = 0; i < corpus.numDocs(); i++)
					modelParams.setb(i, sliceB.sample(modelParams.getb(i), i));
			}
		}else{
			ArmsBSampler armsB =  new ArmsBSampler(stables, modelParams.geta(), 1.0, 1.0);
			if(doOneB){
				modelParams.setb(armsB.sample(modelParams.getb(0)));
			}else
				for(int i = 0; i < corpus.numDocs(); i++)
					modelParams.setb(i, armsB.sample(modelParams.getb(i), i));
		}
	}
	
	/**
	 * Optimise symetric alphas
	 * 
	 */
	public void optimiseSymmetricAlpha(){
		int numDocs = corpus.numDocs();
		double[][] observations = new double[numDocs][modelParams.numTopics()];
		double[] observationLengths = new double[numDocs];
		for(int i = 0; i < numDocs; i++){
			observationLengths[i] = stables.TIJ[i][0];
			for(int k = 0; k < modelParams.numTopics(); k++)
				observations[i][k] = stables.TIJK[i][0][k];
		}
		double newAlpha = 0;
		if(doPolya){
			newAlpha = AGOptimizer.sym_polya_fit(observations, 
												 observationLengths, 
												 numDocs, modelParams.numTopics(), 
												 modelParams.getAlpha(0));
		}else{
			newAlpha = AGOptimizer.sym_polya_fit_newton(observations, 
														observationLengths, 
														numDocs, modelParams.numTopics(), 
														modelParams.getAlpha(0));
		}
		modelParams.setAlpha(newAlpha);
		if(ModelParams.verboseLevel >= 5000)
			System.out.printf("New alpha: %.6f, new alphaSum: %.6f\n", 
							   newAlpha, modelParams.getAlphaSum());
	}
	
	/**
	 * Optimise symmetric gammas
	 */
	public void optimiseSymmetricGamma()
	{
		double[][] observations = new double[modelParams.numTopics()][modelParams.numTypes()];
		double[] observationLengths = new double[modelParams.numTopics()];
		for(int k = 0; k < modelParams.numTopics(); k++){
			observationLengths[k] = stables.MK[k];
			for(int w = 0; w < modelParams.numTypes(); w++)
				observations[k][w] = stables.MKW[k][w];
		}
		double newGamma = 0;
		if(doPolya){
			newGamma = AGOptimizer.sym_polya_fit(observations, 
												observationLengths, 
												modelParams.numTopics(), 
												modelParams.numTypes(), 
												modelParams.getGamma(0));
		}else{
			newGamma = AGOptimizer.sym_polya_fit_newton(observations, 
														observationLengths, 
														modelParams.numTopics(), 
														modelParams.numTypes(), 
														modelParams.getGamma(0));
		}
		modelParams.setGamma(newGamma);
		if(ModelParams.verboseLevel >= 5000)
			System.out.printf("new Gamma: %.6f, new GammaSum: %.6f\n", 
								newGamma, modelParams.getGammaSum());
	}
	
	/**
	 * Return the training perplexity
	 * @return
	 */
	public double perplexity(){ return perplexity(corpus); }
	
	/**
	 * Return perlexity of a given corpus.
	 * 
	 * @param c
	 * @return
	 */
	public double perplexity(Corpus c){
		double val, perpVal = 0;
		int i, j, n, k, w;
		//Compute topic distribution 
		topicDists.compute(stables, modelParams);
		//Compute per-topic word distribution
		if(!modelParams.isPhiGiven())
			this.computePhis();
		for(i = 0; i < c.numDocs(); i++){
			Document doc = c.getDoc(i);
			for(j = 0; j < doc.numTPs(); j++){
				TextPassage tp = doc.getTP(j);
				for(n = 0; n < tp.size(); n++){
					val = 0;
					w = tp.getWord(n);
					for(k = 0; k < modelParams.numTopics(); k++)
						val += modelParams.getPhi(k, w) 
									* topicDists.getPassageTopicDist(i, j, k);
					perpVal += Math.log(val);
				}
			}
		}
		perpVal = Math.exp(-perpVal/c.numWords());
		if(!SpecialFuns.isnormal(perpVal) && perpVal < 0)
			throw new RuntimeException("Illegal perplexity value: "+perpVal);
		return perpVal;
	}
	
	public void writeTRSamplingInfo(Vocabulary voc){
		String folder = modelParams.root() + File.separator + "seqldaTrainingInfo";
		(new File(folder)).mkdirs();
		//Save topic assigment
		ass.writeWordAndTopic(folder, voc);
		//save modelParams
		if(!modelParams.isPhiGiven())
			this.computePhis();
		modelParams.writeParameters(folder);
		//save top words per topic
		String str = folder + File.separator + "seqlda-top-words-per-topic.log";
		modelParams.saveTopicByWords(str, voc);
		//save topic word counts for topic evaluation
		str = folder + File.separator + "seqlda-topic-word-counts.log";
		stables.saveTopicByWordCountMatrix(str, voc);
		//save topic distributions
		topicDists.compute(stables, modelParams);
		topicDists.writeTopicDistribution(folder);
	}
	
	public void writeTESamplingInfo(Vocabulary voc){
		String folder = modelParams.root();
		//Save topic assigment
		ass.writeWordAndTopic(folder, voc);
		//save topic distributions
		topicDists.compute(stables, modelParams);
		topicDists.writeTopicDistribution(folder);
	}
}