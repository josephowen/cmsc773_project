package model;

import java.io.Serializable;

import states.ModelParams;
import states.StirRatioCache;
import util.Vector;
import util.SpecialFuns;
import util.StirNum;
import data.Corpus;

/**
 * 
 * @author Du Lan
 *
 */
public class CollapseSampler extends TopicSampler implements Serializable
{
	//Serialization
	private static final long serialVersionUID = 1L;
	private static final int WINSIZE = 5;
	private double[] tableProbs;
	private int[][] zeroTrace;
	private int tmpn, tmpt;
	
	public CollapseSampler(Corpus corpus, ModelParams modelParams, StirRatioCache cache) 
	{
		super(corpus, modelParams, cache);	
		zeroTrace = new int[corpus.numDocs()][modelParams.numTopics()];
		initZeroTrace();
		if(ModelParams.debug)
			System.out.println("\n Run collpased sampling algorithm!!!\n");
	}
	
	/**
	 * 
	 */
	public void reset() {
		stables.reset();
		initialiseStates();
		initZeroTrace();
		System.gc();
	}
	
	/**
	 * For each document, check from which text passage the table
	 * count of a topic is zero.
	 */
	private void initZeroTrace() {
		for(int i = 0; i < corpus.numDocs(); i++)
			Vector.fill(zeroTrace[i], Integer.MAX_VALUE);
		for(int i = 0; i < corpus.numDocs(); i++){
			for(int k = 0; k < modelParams.numTopics(); k++){
				for(int j = 0; j < corpus.getDoc(i).numTPs(); j++){
					if(stables.TIJK[i][j][k] == 0){
						zeroTrace[i][k] = j;
						break;
					}
				}
				if(ModelParams.debug)
					checkTraceVariable(i, k);
			}
		}
	}
	
	/**
	 * Check for each topic, the table counts at subsequent
	 * text passages from zeroTraced passage are zero.
	 * @param i
	 * @param k
	 */
	private void checkTraceVariable(final int i, final int k){
		for(int jj = zeroTrace[i][k]; jj < stables.NTIJ[i].length; jj++){
			assert stables.TIJK[i][jj][k] == 0;
			assert stables.NTIJK[i][jj][k] == 0;
		}	
	}
	
	/**
	 * Sample topic for a word, and sample the corresponding table count.
	 * 
	 * @return
	 */
	protected int sample(final int i, final int j, final int w, int k){
		k = sampleTopic(i, j, w, k);
		if(stables.NTIJK[i][j][k] > 1)
			sampleTableCount(i, j, k);
		return k;
	}
	
	/**
	 * Return the sampled topic.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param w
	 *            word index
	 * @param k
	 *            topic index
	 * @return
	 */
	private int sampleTopic(final int i, final int j, final int w, int k){
		removeWord(i, j, k, w);
		for(k = 0; k < modelParams.numTopics(); k++)
			topicProbs[k] = topicProb(i, j, w, k);
		k = nextDiscrete(topicProbs);		
		addWord(i, j, k, w);
		return k;
	}
	
	/**
	 * Remove a word.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic index
	 * @param w
	 *            word index
	 */
	private void removeWord(final int i, final int j, final int k, final int w){
		if(stables.NTIJK[i][j][k] == stables.TIJK[i][j][k]) {
			stables.adjustTable(i, j, k, -1);
			if(stables.TIJK[i][j][k] == 0) {
				zeroTrace[i][k] = j;
				assert stables.NTIJK[i][j][k] == 1;
			}
			for(int jj = j-1; jj >= 0; jj--){
				if(stables.NTIJK[i][jj][k] < stables.TIJK[i][jj][k]){
					assert stables.NTIJK[i][jj][k] - stables.TIJK[i][jj][k] == -1;
					stables.adjustTable(i, jj, k, -1);
					if(stables.TIJK[i][jj][k] == 0){
						zeroTrace[i][k] = jj;
						assert stables.NTIJK[i][jj][k] == 0;
					}
				} else {
					assert stables.NTIJK[i][jj][k] >= stables.TIJK[i][jj][k];
					break;
				}
			}
		}
		stables.adjustCust(i, j, w, k, -1);
		if(ModelParams.debug)
			checkTraceVariable(i, k);
	}
	
	/**
	 * Add a word back.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic index
	 * @param w
	 *            word index
	 */
	private void addWord(final int i, final int j, final int k, final int w){
		int u = zeroTrace[i][k];
		if(u <= j){
			for(int jj = j; jj >= u; jj--){
				assert stables.TIJK[i][jj][k] == 0;
				stables.adjustTable(i, jj, k, 1);
			}
			zeroTrace[i][k] = j+1;
		}
		stables.adjustCust(i, j, w, k, 1);
		if(ModelParams.debug)
			checkTraceVariable(i, k);
	}
	
	/**
	 * return topic probability for a given word.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param w
	 *            word index
	 * @param k
	 *            topic index
	 * @return
	 */
	private double topicProb(final int i, final int j, 
						     final int w,  final int k)
	{
		double val;
		double b = modelParams.getb(i);
		double a = modelParams.geta();
		int u = zeroTrace[i][k];
		if(modelParams.isPhiGiven())
			val = modelParams.getPhi(k, w);
		else
			val = (modelParams.getGamma(w) + stables.MKW[k][w])
					/(modelParams.getGammaSum() + stables.MK[k]);
		if(u <= j){
			if(u == 0){
				val *= (modelParams.getAlpha(k) + stables.TIJK[i][0][k])
						/(modelParams.getAlphaSum() + stables.TIJ[i][0]);
			}else{
				val *= cache.getStirRatio(stables.NTIJK[i][u-1][k], stables.TIJK[i][u-1][k])
								/(b + stables.NTIJ[i][u-1]);
			}
			for(int jj = u; jj <= j; jj++)
				val *= (b + a*stables.TIJ[i][jj])/(b + stables.NTIJ[i][jj]);
		}else{
			val *= cache.getStirRatio(stables.NTIJK[i][j][k], stables.TIJK[i][j][k])
						/(b + stables.NTIJ[i][j]);
		}
		if(!SpecialFuns.isnormal(val) || val < 0)
			throw new RuntimeException("Illegal topicProb in collapse sampler!!! val = "+val);
		return val;
	}
	
	
	/**
	 * Sample table counts, given all the topic assignments.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic index
	 */
	private void sampleTableCount(final int i, final int j, final int k){
		int m, diff;
		int lowerLimit = 1;
		if(j > 0){
			diff = stables.TIJK[i][j-1][k]-stables.NIJK[i][j-1][k];
			if(diff > 1)
				lowerLimit = diff;
		}
		tmpn = stables.NTIJK[i][j][k];
		tmpt = stables.TIJK[i][j][k];	
		int upper = tmpt + WINSIZE;
		if(upper > tmpn) 
			upper = tmpn;
		int lower = tmpt - WINSIZE;
		if(lower < lowerLimit)
			lower = lowerLimit;
		int winlength = upper - lower + 1;
		tableProbs = new double[winlength];
		for(m = 0; m < winlength; m++){
			diff = lower + m - tmpt;
			tableProbs[m] = logTableProb(i, j, k, diff);
		}
		double max = Vector.max(tableProbs);
		double tmpSum = 0, tmp;
		for(m = 0; m < winlength; m++) {
			tmp = tableProbs[m] - max;
			if(tmp > -20)
				tmpSum += Math.exp(tmp);
		}
		for(m = 0; m < winlength; m++) {
			tmp = tableProbs[m] - max;
			if(tmp > -20)
				tableProbs[m] =  Math.exp(tmp)/tmpSum;
			else
				tableProbs[m] = 0.0;
		}
		diff = nextDiscrete(tableProbs) + lower - tmpt;
		stables.adjustTable(i, j, k, diff);
	}
	
	/**
	 * Return the log table probabilities.
	 * 
	 * @param i
	 *            document index
	 * @param j
	 *            text passage index
	 * @param k
	 *            topic
	 * @param change
	 *            the difference between new table count and old table coute
	 * @return
	 */
	private double logTableProb(final int i, final int j, final int k, final int change){
		double val = SpecialFuns.logPochSym(modelParams.getb(i), 
											modelParams.geta(), 
											stables.TIJ[i][j]+change)
					 + StirNum.logSN(stables.NTIJK[i][j][k], stables.TIJK[i][j][k]+change);
		if(j == 0){
			val += SpecialFuns.logGamma(modelParams.getAlpha(k) + stables.TIJK[i][0][k] + change)
				 	- SpecialFuns.logGamma(modelParams.getAlphaSum() + stables.TIJ[i][0] + change);
		}else{
			val += StirNum.logSN(stables.NTIJK[i][j-1][k]+change, stables.TIJK[i][j-1][k])
					- SpecialFuns.logPochSym(modelParams.getb(i), 1.0, stables.NTIJ[i][j-1]+change);
		}
		if(!SpecialFuns.isnormal(val))
			throw new RuntimeException("Illegal table count prob in collapse sampler!!!");
		return val;
	}
	
	/**
	 * Return the model log likelihood
	 * 
	 * @return
	 */
	public double logLikelihood() {
		double logLikelihood = globalLLL();
		for (int i = 0; i < corpus.numDocs(); i++) {
			logLikelihood += SpecialFuns.logGamma(modelParams.getAlphaSum())
							 - SpecialFuns.logGamma(modelParams.getAlphaSum() + stables.TIJ[i][0]);
			for (int k = 0; k < modelParams.numTopics(); k++)
				logLikelihood += SpecialFuns.logGamma(modelParams.getAlpha(k) + stables.TIJK[i][0][k])
				 				 - SpecialFuns.logGamma(modelParams.getAlpha(k));
			for (int j = 0; j < corpus.getDoc(i).numTPs(); j++) {	
				logLikelihood += SpecialFuns.logPochSym(modelParams.getb(i), modelParams.geta(), 
														stables.TIJ[i][j])
									- SpecialFuns.logPochSym(modelParams.getb(i), 1.0, stables.NTIJ[i][j]);
				for (int k = 0; k < modelParams.numTopics(); k++)
					logLikelihood += StirNum.logSN(stables.NTIJK[i][j][k], stables.TIJK[i][j][k]);
			}
		}
		logLikelihood /= - corpus.numWords();
		if (!SpecialFuns.isnormal(logLikelihood)) {
			throw new RuntimeException("Collapse sampler has illegal log model likelihood!!!");
		}
		return logLikelihood;
	}
	
	public void yap(Object obj) {
		System.out.println(obj);
	}
}
