package data;

import gnu.trove.TIntObjectHashMap;
import java.io.*;
import java.util.ArrayList;
import cc.mallet.pipe.iterator.FileIterator;

/**
 * 
 * @author Lan Du
 * @version 1.0 Build Dec 17, 2012
 */
public class Corpus implements Serializable {
	//Serialization
	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;

	private ArrayList<Document> docList;
	
	private int wordTotal;
	private int tpTotal;
	private int goldSegTotal;
	
	private int maxDocSizeInToken;
	private int maxDocSizeInTP;
	private int maxDocSizeInGoldSeg;
	private double aveDocSizeInToken;
	private double aveDocSizeInTP;
	private double aveDocSizeInGoldSeg;
	private int minDocSizeInToken = Integer.MAX_VALUE;
	private int minDocSizeInTP = Integer.MAX_VALUE;
	private int minDocSizeInGoldSeg = Integer.MAX_VALUE;
	//Text passage size
	private int maxTPSizeInToken;
	private int minTPSizeInToken = Integer.MAX_VALUE;
	//Segment size in terms of text passages
	private int maxSegSizeInTP;
	private int minSegSizeInTP = Integer.MAX_VALUE;
	
	public Corpus(){
		docList = new ArrayList<Document>();
	}
	
	/**
	 * 
	 * @param docList
	 */
	public Corpus(ArrayList<Document> docList){
		this.docList = docList;
		for(int i = 0; i < docList.size(); i++)
			countEachDoc(docList.get(i));
		aveDocSizeInToken = (double) wordTotal/docList.size();
		aveDocSizeInTP = (double) tpTotal/docList.size();
		aveDocSizeInGoldSeg = (double) goldSegTotal/docList.size();
	}
	
	/**
	 * Create a corpus from a given file directory
	 * 
	 * @param dataDir
	 * @param voc
	 * @param fileNameExt
	 */
	public Corpus(String dataDir, 
				  Vocabulary voc, 
				  String fileNameExt,
				  String boundaryIndicator) 
	{
		this();
		Document.setBoundaryIndicator(boundaryIndicator);
		FileIterator iterator = new FileIterator(new File(dataDir),
												 new DataFileFilter(fileNameExt));
		while (iterator.hasNext()) {
			File dataFile = iterator.nextFile();
			Document doc = new Document(dataFile, docList.size(), voc);
			if(doc.numTPs() > 0){
				docList.add(doc);
				//compute statistics
				countEachDoc(doc);
			}
		}
		aveDocSizeInToken = (double) wordTotal/docList.size();
		aveDocSizeInTP = (double) tpTotal/docList.size();
		aveDocSizeInGoldSeg = (double) goldSegTotal/docList.size();
	}
	
	private void countEachDoc(Document doc){
		wordTotal += doc.numWords();
		if(doc.numWords() > maxDocSizeInToken)
			maxDocSizeInToken = doc.numWords();
		if(doc.numWords() < minDocSizeInToken)
			minDocSizeInToken = doc.numWords();
		
		tpTotal += doc.numTPs();
		if(doc.numTPs() > maxDocSizeInTP)
			maxDocSizeInTP = doc.numTPs();
		if(doc.numTPs() < minDocSizeInTP)
			minDocSizeInTP = doc.numTPs();
		
		goldSegTotal += doc.numTrueSegments();
		if(doc.numTrueSegments() > maxDocSizeInGoldSeg)
			maxDocSizeInGoldSeg = doc.numTrueSegments();
		if(doc.numTrueSegments() < minDocSizeInGoldSeg)
			minDocSizeInGoldSeg = doc.numTrueSegments();
		
		if(doc.maxTPsize() > maxTPSizeInToken)
			maxTPSizeInToken = doc.maxTPsize();
		if(doc.minTPsize() < minTPSizeInToken)
			minTPSizeInToken = doc.minTPsize();
		
		if(doc.maxTrueSegSize() > maxSegSizeInTP)
			maxSegSizeInTP = doc.maxTrueSegSize();
		if(doc.minTrueSegSize() < minSegSizeInTP)
			minSegSizeInTP = doc.minTrueSegSize();
	}

	public int numWords() {
		return wordTotal;
	}

	public int numDocs() {
		return docList.size();
	}
	
	public int numGoldSegs(){
		return goldSegTotal;
	}
	
	public int numTPs(){
		return tpTotal;
	}
	
	public int maxDocSizeInWord(){
		return maxDocSizeInToken;
	}
	
	public int maxTPsize(){
		return maxTPSizeInToken;
	}

	/**
	 * 
	 * @param docIndex
	 *            the index of document in a corpus.
	 * @return a document with specified index.
	 */
	public Document getDoc(int docIndex) {
		return docList.get(docIndex);
	}

	/**
	 * Randomly permutate the text passage list
	 * of each document.
	 */
	public void randomShuffle(){
		for(Document doc : docList){
			doc.shuffle();
		}
	}

	/**
	 * Save a corpus object in a file.
	 * 
	 * @param fileName
	 */
	public void saveObject(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeInt (CURRENT_SERIAL_VERSION);
			oos.writeInt(docList.size());
			for(int i = 0; i < docList.size(); i++)
				oos.writeObject(docList.get(i));
			oos.flush(); oos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Read a corpus object from a file.
	 * 
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unused")
	public void readObject(String fileName) {
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			int version = ois.readInt();
			int numDocs = ois.readInt();
			for(int i = 0; i < numDocs; i++){
				Document doc = (Document)ois.readObject();
				docList.add(doc);
				countEachDoc(doc);
			}
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		aveDocSizeInToken = (double) wordTotal/docList.size();
		aveDocSizeInTP = (double) tpTotal/docList.size();
		aveDocSizeInGoldSeg = (double) goldSegTotal/docList.size();
	}

	/**
	 * Save the gold-standard segmentation.
	 * 
	 * @param fileName
	 */
	public void writeGoldSegmentaionObject(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeInt (CURRENT_SERIAL_VERSION);
			out.writeInt(docList.size());
			for (int i = 0; i < docList.size(); i++)
				out.writeObject(docList.get(i).copyOfGoldSegmentation());
			out.flush();
			out.close();
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	public TIntObjectHashMap<short[]> readGoldSegmentaionObject(String fileName) {
		TIntObjectHashMap<short[]> goldSegmentation = new TIntObjectHashMap<short[]>();
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fis);
			int version = in.readInt ();
			int numDocs = in.readInt();
			for (int i = 0; i < numDocs; i++)
				goldSegmentation.put(i, (short[])in.readObject());
			in.close();
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		return goldSegmentation;
	}

	/**
	 * Need to be modified
	 * 
	 * @param dir
	 */
	public void saveSegmentedDocs(String dir) {

	}

	/**
	 * Print the corpus statistics in console.
	 */
	public void printCorpusStats() 
	{
		System.out.println("==============================================================");
		System.out.println("total number of docs: " + docList.size());
		System.out.println("total number of words: " + wordTotal);
		System.out.println("total number of true segments: " + goldSegTotal);
		System.out.println("total number of text passage: " + tpTotal);
		System.out.println();

		System.out.println("largest doc size in token: " + maxDocSizeInToken);
		System.out.println("smallest doc size in token: " + minDocSizeInToken);
		System.out.println("avarage doc size in token: " + aveDocSizeInToken);
		System.out.println();
		
		System.out.println("largest doc size in text passage: " + maxDocSizeInTP);
		System.out.println("smallest doc size in text passage: " + minDocSizeInTP);
		System.out.println("avarage doc size in text passage: " + aveDocSizeInTP);
		System.out.println("");
		
		System.out.println("average number of words in each text passage: " + wordTotal / tpTotal);
		System.out.println("maximum text passage size: "+maxTPSizeInToken+" words.");
		System.out.println("minimum text passage size: "+minTPSizeInToken+" words.");
		System.out.println();
		
		if(goldSegTotal > 0){
			System.out.println("largest doc size in gold seg: " + maxDocSizeInGoldSeg);
			System.out.println("smallest doc size in gold seg: " + minDocSizeInGoldSeg);
			System.out.println("avarage doc size in gold seg: " + aveDocSizeInGoldSeg);
			System.out.println();
			
			System.out.println("average number of words in each true segment: "+ wordTotal / goldSegTotal);
			System.out.println("average number of text passages in each true segment: " + tpTotal / goldSegTotal);
			System.out.println("Maximum segment size : "+maxSegSizeInTP+" text passages.");
			System.out.println("Minimum segment size : "+minSegSizeInTP+" text passages.");
		}
		System.out.println("==============================================================");
	}
}
