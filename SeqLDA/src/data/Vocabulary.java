package data;

import java.util.*;
import java.io.*;

import malletWrap.*;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.types.Alphabet;
import cc.mallet.types.InstanceList;
import cc.mallet.util.CharSequenceLexer;

/**
 * 
 * @author Lan Du
 * @version 1.0 Build Dec 17, 2012
 * @serial 0
 */
public class Vocabulary implements Serializable {
	//Serialization
	private static final long serialVersionUID = 1L;
	private static final int CURRENT_SERIAL_VERSION = 0;
	// TreeMap->key = type; TreeMap->value = vocabulary index
	private TreeBidiMap types2Index;
	/**
	 * 
	 */
	public Vocabulary() {
		types2Index = new TreeBidiMap();
	}

	/**
	 * 
	 * @param input 
	 * 			 	the corpus directory
	 * @param stopWordList 
	 * 				a file contains a list of stopd words
	 * @param fileNameExt 
	 * 				an array of file extesions
	 */
	@SuppressWarnings("rawtypes")
	public Vocabulary(String input, 
					  String stopWordList, 
					  String[] fileNameExts) 
	{
		this();
		Pipe pipe = buildPipe(stopWordList);
		FileIterator iterator = new FileIterator(new File(input), new DataFileFilter(fileNameExts));
		InstanceList instances = new InstanceList(pipe);
		instances.addThruPipe(iterator);
		Alphabet alphabet = instances.getAlphabet();
		Iterator ite = alphabet.iterator();
		while (ite.hasNext()) {
			String type = (String) ite.next();
			int index = alphabet.lookupIndex(type);
			types2Index.put(type, new Integer(index));
		}
	}

	/**
	 * 
	 * @param input
	 *            the corpus directory
	 * @param stopWordList
	 *            a file contains a list of stop words
	 * @param fileNameExt
	 *            an array of file extensions
	 * @param minFreq
	 *            minimum document frequence
	 * @param topNumWords
	 *            the number of most frequent words to be removed
	 */
	@SuppressWarnings("rawtypes")
	public Vocabulary(String input, 
					  String stopWordList, 
					  String[] fileNameExts,
					  int minFreq, int topNumWords) 
	{
		this();
		Pipe pipe = buildPipe(stopWordList);
		FileIterator iterator = new FileIterator(new File(input), new DataFileFilter(fileNameExts));
		InstanceList instances = new InstanceList(pipe);
		instances.addThruPipe(iterator);

		FeatureDFPipe featureDFPipe = new FeatureDFPipe(instances.getAlphabet(), null);
		InstanceList newInstances = new InstanceList(featureDFPipe);
		newInstances.addThruPipe(instances.iterator());
		int maxFreq = newInstances.size() - 1;
		if (topNumWords > 0)
			maxFreq = newInstances.size() - topNumWords;
		if (minFreq == 0)
			minFreq = 1;
		Alphabet alphabet = featureDFPipe.getPrunedAlphabet(minFreq, maxFreq);
		Iterator ite = alphabet.iterator();
		while (ite.hasNext()) {
			String type = (String) ite.next();
			int index = alphabet.lookupIndex(type);
			types2Index.put(type, new Integer(index));
		}
	}

	/**
	 * Build pipeline using the Mallet pipe
	 * 
	 * @param stopWordList
	 * @return
	 */
	private Pipe buildPipe(String stopWordList) {
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
		pipeList.add(new Input2CharSequence("UTF-8"));
		pipeList.add(new CharSequence2TokenSequence(CharSequenceLexer.LEX_WORDS));
		pipeList.add(new TokenSequenceLowercase());
		pipeList.add(new TokenSequenceRemoveNonAlpha());
		if (stopWordList == null)
			pipeList.add(new TokenSequenceRemoveStopwords(false, false));
		else
			pipeList.add(new TokenSequenceRemoveStopwords(new File(stopWordList), 
										"UTF-8", false, false, false));
		pipeList.add(new TokenSequence2FeatureSequence());
		Pipe pipe = new SerialPipes(pipeList);
		return pipe;
	}

	/**
	 * 
	 * @return the size of the vocabulary.
	 */
	public int size() {
		return types2Index.size();
	}

	/**
	 * 
	 * @param type
	 *            token type
	 * @return
	 */
	public boolean contains(String type) {
		return types2Index.containsKey(type);
	}

	/**
	 * 
	 * @param type
	 * @return the type index in the vocabulary.
	 */
	public Integer getTypeIndex(String type) {
		return (Integer) types2Index.get(type);
	}

	/**
	 * 
	 * @param index
	 *            the type index
	 * @return the string of the type
	 */
	public String getType(Integer index) {
		return (String) types2Index.getKey(index);
	}

	/**
	 * Read a vocabulary from a file, which has the following format:
	 * 
	 * "String:Integer"
	 * 
	 * @param fileName
	 */
	public void read(String fileName) {
		try {
			FileReader freader = new FileReader(new File(fileName));
			BufferedReader breader = new BufferedReader(freader);
			String str = breader.readLine();
			while (str != null) {
				String[] strs = str.split(":");
				types2Index.put(strs[0], new Integer(strs[1]));
				str = breader.readLine();
			}
			breader.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Save the vocabulary in a file with the following format: 
	 * 
	 * "String:Integer"
	 * 
	 * @param fileName
	 */
	public void save(String fileName) {
		try {
			FileWriter fwriter = new FileWriter(new File(fileName));
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			for (Object key : types2Index.keySet()) {
				String type = (String) key;
				Integer index = (Integer) types2Index.get(key);
				bwriter.write(type + ":" + index);
				bwriter.newLine();
			}
			bwriter.flush();
			bwriter.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void writeObject(String fileName){
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeInt (CURRENT_SERIAL_VERSION);
			oos.writeObject(types2Index);
			oos.flush(); oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	public void readObject(String fileName) {
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			int version = ois.readInt();
			types2Index = (TreeBidiMap)ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clear the vocabulary.
	 */
	public void clear() {
		types2Index.clear();
	}
}
