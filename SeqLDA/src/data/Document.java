package data;

import gnu.trove.TShortArrayList;
import java.util.ArrayList;
import java.util.Collections;
import java.io.*;

/**
 * 
 * @author Lan Du
 * @version 1.0 Build Dec 17, 2012
 */
public class Document implements Serializable {
	//Serialization
	private static final long serialVersionUID = 1L;
	//The minimum text passage size
	private static int MINTPSIZE = 5;
	//Segment boundary indicator
	private static String SEGINDICATOR = null;
	//Class variables
	private String docName;
	private final int docIndex;
	private int textPassageCount;
	private int wordCount;
	private int trueSegCount;
	private ArrayList<TextPassage> textPassageList;
	private TShortArrayList goldSegmentation;
	//Count max/min text passage size
	private int maxTpSize = 0;
	private int minTpSize = Integer.MAX_VALUE;
	//Count max/min true segment size
	private int maxTrueSegSize = 0;
	private int minTrueSegSize = Integer.MAX_VALUE;
	/**
	 * Create a document with specified text passage list and gold
	 * segmentation.
	 * 
	 * @param textPassageList
	 * @param goldSegmentation
	 * @param docIndex
	 * @param docName
	 */
	public Document(ArrayList<TextPassage> textPassageList,
					TShortArrayList goldSegmentation, 
					int docIndex, 
					String docName) 
	{
		this.textPassageList = textPassageList;
		this.goldSegmentation = goldSegmentation;
		this.docIndex = docIndex;
		this.docName = docName;
		textPassageCount = textPassageList.size();
		for (int j = 0; j < textPassageCount; j++) {
			int tpSize = textPassageList.get(j).size();
			wordCount += tpSize;
			if (goldSegmentation != null)
				trueSegCount += goldSegmentation.get(j);
			if(tpSize > maxTpSize)
				maxTpSize = tpSize;
			if(tpSize < minTpSize)
				minTpSize = tpSize;
		}
		if (goldSegmentation != null)
			countTrueSegSize();
	}

	/**
	 * Create a document from a file.
	 * Note there must be a blank line between two text
	 * passages.
	 * 
	 * @param file
	 *            document file
	 * @param docIndex
	 *            document index in a corpus
	 * @param voc
	 *            vocabulary
	 */
	public Document(File file, int docIndex, Vocabulary voc) 
	{
		this.docIndex = docIndex;
		docName = file.getName();
		int dotIndex = docName.lastIndexOf('.');
		if(dotIndex >= 0)
			docName=docName.substring(0,dotIndex);

		textPassageList = new ArrayList<TextPassage>();
		if(SEGINDICATOR != null)
			goldSegmentation = new TShortArrayList();
		try {
			BufferedReader breader = new BufferedReader(new FileReader(file));
			String str = breader.readLine();
			while (str != null) {
				if (!str.equals("")) {
					String tmp = breader.readLine();
					while (tmp != null && !tmp.equals("")) {
						str += "\n" + tmp;
						tmp = breader.readLine();
					}
					if (SEGINDICATOR != null && str.equals(SEGINDICATOR)) {
						if (textPassageCount > 0) {
							goldSegmentation.set(textPassageCount - 1, (short) 1);
							trueSegCount++;
						}
					} else {
						TextPassage tp = new TextPassage(str, textPassageCount, voc);
						int tpSize = tp.size();
						if (tpSize >= MINTPSIZE) {
							textPassageList.add(tp);
							assert textPassageCount == textPassageList.indexOf(tp);
							wordCount += tpSize;
							textPassageCount++;
							if(SEGINDICATOR != null)
								goldSegmentation.add((short) 0);
							if(tpSize > maxTpSize)
								maxTpSize = tpSize;
							if(tpSize < minTpSize)
								minTpSize = tpSize;
						}
					}
				}
				str = breader.readLine();
			}
			breader.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}	
		assert textPassageCount == textPassageList.size();
		if(SEGINDICATOR != null){
			countTrueSegSize();
			assert textPassageCount == goldSegmentation.size();
		}
	}

	/**
	 * Set the segmentation boundary indicator in the text
	 * 
	 * @param indicator
	 *            the boundary indicator
	 */
	public static void setBoundaryIndicator(String indicator) {
		SEGINDICATOR = indicator;
	}
	
	public static void setMinPassageSize(int size){
		MINTPSIZE = size;
	}
	
	private void countTrueSegSize(){
		int segSize = 0;
		for(int j = 0; j < textPassageCount; j++) {
			segSize++;
			if(goldSegmentation.get(j) == 1){
				if(segSize > maxTrueSegSize)
					maxTrueSegSize = segSize;
				if(segSize < minTrueSegSize)
					minTrueSegSize = segSize;
				segSize = 0;
			}
		}
	}

	/**
	 * 
	 * @return the number of gold-standard segments
	 */
	public int numTrueSegments() {
		return trueSegCount;
	}

	/**
	 * 
	 * @return a copy of the gold-standard segmentation
	 */
	public short[] copyOfGoldSegmentation() {
		short[] reference = new short[textPassageCount];
		for (int tpID = 0; tpID < textPassageCount; tpID++)
			reference[tpID] = goldSegmentation.get(tpID);
		return reference;
	}

	/**
	 * return the gold topic shift.
	 * @param tpIndex
	 * @return
	 */
	public short getGoldTopicShift(int tpIndex) {
		return goldSegmentation.get(tpIndex);
	}

	/**
	 * @return the document index
	 */
	public int getDocIndex() {
		return docIndex;
	}

	/**
	 * 
	 * @return the document name
	 */
	public String getDocName() {
		return docName;
	}

	/**
	 * 
	 * @return the number of tokens
	 */
	public int numWords() {
		return wordCount;
	}

	/**
	 * 
	 * @return the number of text passages
	 */
	public int numTPs() {
		return textPassageCount;
	}

	/**
	 * 
	 * @param tpIndex
	 *            the TextPassage index
	 * @return a TextPassage
	 * @see TextPassage
	 */
	public TextPassage getTP(int tpIndex) {
		return textPassageList.get(tpIndex);
	}
	
	public int maxTPsize(){
		return maxTpSize;
	}
	
	public int minTPsize(){
		return minTpSize;
	}
	
	public int maxTrueSegSize(){
		return maxTrueSegSize;
	}
	
	public int minTrueSegSize(){
		return minTrueSegSize;
	}
	
	/**
	 * Randomly permutate the text passage
	 * list
	 */
	public void shuffle(){
		Collections.shuffle(textPassageList);
	}

	/**
	 * Save a document with a specified segmentation.
	 * 
	 * @param fileName
	 * @param segmentation
	 *            a segmentation
	 */
	public void saveDocWithSegmentations(String fileName, short[] segmentation) {
		assert segmentation.length == textPassageCount : "rhos.length != textPassageCount";
		try {
			FileWriter fw = new FileWriter(new File(fileName));
			for (int tpID = 0; tpID < segmentation.length; tpID++) {
				TextPassage tp = textPassageList.get(tpID);
				assert tpID == tp.getTPIndex();
				fw.write(tpID + ": " + tp.text() + "\n\n");
				if (segmentation[tpID] == 1) {
					fw.write(SEGINDICATOR + "\n\n");
				}
				fw.flush();
			}
			fw.flush();
			fw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
