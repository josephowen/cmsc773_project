import nltk
from nltk.collocations import *
import string
import os
import sys
import warnings
warnings.filterwarnings("ignore")

ignored_words = nltk.corpus.stopwords.words('english')

if len(sys.argv) > 1:
    outFile = open(sys.argv[1], 'w')
else:
    outFile = sys.stdout

def getPostWords(line):
    thisLine = []
    line = line.split("\t")
    words = line[-1].strip('"').split()
    for word in words:
        if not word in string.punctuation:
            thisLine.append(word.lower())
    return thisLine

def printPairs(pairs):
    maxWord = 0
    for item in pairs:
        maxWord = max(maxWord, len(item[0][0] + " " + item[0][1]))

    spaces = maxWord + 3
    for item in pairs:
        bigram = item[0][0] + " " + item[0][1]
        outFile.write(bigram + " " * (spaces-len(bigram)) + str(item[1]) + "\n")

def getBigrams(docs, freqFilter=0, numToShow=25, ignore_stopwords=True):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_documents(docs)

    if ignore_stopwords:
        finder.apply_word_filter(lambda w: w in ignored_words)

    if freqFilter != 0:
        finder.apply_freq_filter(freqFilter)
    best_freq = finder.score_ngrams(bigram_measures.raw_freq)[:numToShow]
    for i, item in enumerate(best_freq):
        best_freq[i] = (item[0], finder.ngram_fd[item[0]])
    outFile.write("\nFrequency\n")
    printPairs(best_freq)

    #best_pmi = finder.score_ngrams(bigram_measures.pmi)[:numToShow]
    #outFile.write("\nPMI\n")
    #printPairs(best_pmi)


positive = []
negative = []

positiveFile = "../project_materials/reddit/depressed.train.txt"
negativeFile = "../project_materials/reddit-for-project/reddit-all/reddit-all-log.train.txt"

with open(positiveFile) as f:
    for line in f:
        positive.append(getPostWords(line))

with open(negativeFile) as f:
    for i, line in enumerate(f):
        if (i+0)%5 == 0:
            negative.append(getPostWords(line))

outFile.write("POSITIVE:\n")
getBigrams(positive, freqFilter=50, numToShow=100, ignore_stopwords=False)
outFile.write("\n\nNEGATIVE\n")
getBigrams(negative, freqFilter=50, numToShow=100, ignore_stopwords=False)

outFile.close()