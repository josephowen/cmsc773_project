import nltk
from nltk.collocations import *
import string
import os
import sys
import csv
import warnings
warnings.filterwarnings("ignore")

usersFilename = "../users.train.csv"

users_positive = []
users_negative = []

with open(usersFilename, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
            if row[-1] == "1":
                users_positive.append(row[0])
            else:
                users_negative.append(row[0])
        else:
            first = False


ignored_words = nltk.corpus.stopwords.words('english')

if len(sys.argv) > 1:
    outFile = open(sys.argv[1], 'w')
else:
    outFile = sys.stdout

def getDocWords(file):
    doc = []
    with open(file) as f:
        for line in f:
            thisLine = []
            words = line.split()
            for word in words:
                if not word in string.punctuation:
                    thisLine.append(word.lower())
            doc.append(thisLine)
    return doc

def printTriples(pairs):
    maxWord = 0
    for item in pairs:
        maxWord = max(maxWord, len(item[0][0] + " " + item[0][1] + " " + item[0][2]))

    spaces = maxWord + 3
    for item in pairs:
        trigram = item[0][0] + " " + item[0][1] + " " + item[0][2]
        outFile.write(trigram + " " * (spaces-len(trigram)) + str(item[1]) + "\n")

def getTrigrams(docs, freqFilter=0, numToShow=25, ignore_stopwords=True):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = TrigramCollocationFinder.from_documents(docs)

    if ignore_stopwords:
        finder.apply_word_filter(lambda w: w in ignored_words)

    if freqFilter != 0:
        finder.apply_freq_filter(freqFilter)
    best_freq = finder.score_ngrams(bigram_measures.raw_freq)[:numToShow]
    for i, item in enumerate(best_freq):
        best_freq[i] = (item[0], finder.ngram_fd[item[0]])
    outFile.write("\nFrequency\n")
    printTriples(best_freq)

    best_pmi = finder.score_ngrams(bigram_measures.pmi)[:numToShow]
    outFile.write("\nPMI\n")
    printTriples(best_pmi)


positive = []
negative = []

dir = "../project_materials/mypersonality_depression/text/"

for i, user in enumerate(users_negative):
    negative += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break
for i, user in enumerate(users_positive):
    positive += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break

outFile.write("POSITIVE:\n")
getTrigrams(positive, freqFilter=20, numToShow=100, ignore_stopwords=False)
outFile.write("\n\nNEGATIVE\n")
getTrigrams(negative, freqFilter=20, numToShow=100, ignore_stopwords=False)

outFile.close()