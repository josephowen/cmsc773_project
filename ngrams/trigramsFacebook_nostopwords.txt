POSITIVE:

Frequency
1 fly black          113
fly black guy        113
<3 <3 <3             43
home home home       35
can't wait till      33
(: (: (:             32
happy new year       30
x x x                28
john madden john     27
madden john madden   27
la la la             26
i'm pretty sure      25
fuck! fuck! fuck!    24
hi hi hi             23
:d :d :d             21
blah blah blah       21
current mood is:     20
feel like i'm        20
fuzzy navel fuzzy    20
navel fuzzy navel    20
o x o                20
x o x                20

PMI
navel fuzzy navel    10.5850942398
fuzzy navel fuzzy    10.1700567406
fuck! fuck! fuck!    8.8117583164
madden john madden   7.87639249789
hi hi hi             6.24553551055
current mood is:     5.88835308417
john madden john     5.56480634723
blah blah blah       5.28168905069
fly black guy        2.0887149553
1 fly black          1.60753291249
x x x                1.49921326851
x o x                0.780689319656
o x o                0.547592197976
la la la             -0.760779927024
(: (: (:             -1.29787335663
can't wait till      -3.53543016646
home home home       -4.21968993116
happy new year       -4.69626416122
i'm pretty sure      -5.04289200062
<3 <3 <3             -6.0130721455
:d :d :d             -7.80414227052
feel like i'm        -10.2771156524


NEGATIVE

Frequency
<3 <3 <3                           210
today's jeopardy answer            105
xd xd xd                           101
asks today's jeopardy              90
10 10 10                           80
bla bla bla                        77
:d :d :d                           51
cant wait till                     42
happy new year                     39
ha ha ha                           37
bother bother bother               34
bored bored bored                  32
states today's jeopardy            29
na na na                           28
*headdesk* *headdesk* *headdesk*   27
usa! usa! usa!                     25
diana campero vigueras             23
:) :) :)                           22
feel like i'm                      21
... ... ...                        20

PMI
diana campero vigueras             9.91028166402
usa! usa! usa!                     9.19412444453
*headdesk* *headdesk* *headdesk*   8.18924942476
bother bother bother               7.32617663437
bla bla bla                        6.46196688588
asks today's jeopardy              4.70756627263
states today's jeopardy            4.217529944
today's jeopardy answer            4.04499720223
ha ha ha                           2.94945413028
bored bored bored                  1.50810380417
10 10 10                           1.47707931201
cant wait till                     -0.851599972189
... ... ...                        -2.52250424663
xd xd xd                           -2.80380138292
na na na                           -3.29133054397
happy new year                     -4.12445380911
<3 <3 <3                           -4.52734356509
:d :d :d                           -6.34799227145
:) :) :)                           -9.18286409278
feel like i'm                      -9.32887651699
