import nltk
from nltk.collocations import *
import string
import os
import sys
import csv
import warnings
warnings.filterwarnings("ignore")

usersFilename = "../users.train.csv"

users_positive = []
users_negative = []

with open(usersFilename, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
            if row[-1] == "1":
                users_positive.append(row[0])
            else:
                users_negative.append(row[0])
        else:
            first = False


ignored_words = nltk.corpus.stopwords.words('english')

noStopWords = False

if len(sys.argv) > 1:
    if len(sys.argv) == 3:
        fileBase = sys.argv[1]
        dot = fileBase.rfind(".")

        outFilePositive = open(fileBase[:dot]+"_positive"+fileBase[dot:], 'w')
        outFileNegative = open(fileBase[:dot]+"_negative"+fileBase[dot:], 'w')

        if sys.argv[2] == "nostopwords":
            noStopWords = True
    else:
        if sys.argv[1] == "nostopwords":
            noStopWords = True
        else:
            fileBase = sys.argv[1]
            dot = fileBase.rfind(".")

            outFilePositive = open(fileBase[:dot]+"_positive"+fileBase[dot:], 'w')
            outFileNegative = open(fileBase[:dot]+"_negative"+fileBase[dot:], 'w')
else:
    outFilePositive = sys.stdout
    outFileNegative = sys.stdout

emotes=[":)", "(:", ":(", "):",
        "c:", ":c",
        "=)", "(=", "=(", ")=",
        ":-)", "(-:", ":-(", ")-:",
        ":-c", "c-:",
        ";)", "(;", ";(", ");",
        ";-)", "(-;", ";-(", ")-;",
        ":'(", ")':", ":'c",
        ">:(", "):<", ">:c",
        ">=(", ")=<", ">=c",
        ":d", "d:", "=d", "d=",
        ":p", "p:", ";p", "p;",
        "o_o", "o.o",
        "^_^", "^.^", "^^",
        ">.<", ">_<", "><",
        ">.>", "<.<", ">_>", "<_<",
        ":3", "=3",
        "-.-", "-_-",
        ":]", "[:", ":[", "]:",
        "=]", "[=", "=[", "]=",
        ":|", "|:", "=|", "|=",
        "<3"]

def getDocWords(file):
    doc = []
    with open(file) as f:
        for line in f:
            thisLine = []
            words = line.split()
            for word in words:
                if not word in string.punctuation:
                    word = word.lower()

                    #thisLine.append(word.lower())
                    
                    if word in emotes:
                        thisLine.append(word)
                    else:
                        for char in string.punctuation:
                            word = word.strip(char)
                        if len(word) != 0:
                            thisLine.append(word)
                    
            doc += thisLine
    return doc

def printPairs(pairs, outFile):
    maxWord = 0
    maxNum = 0
    for i, item in enumerate(pairs):
        maxWord = max(maxWord, len(item[0]))
        maxNum = max(maxNum, len(str(i)))

    spaces = maxWord + 3
    spaces2 = maxNum + 3
    for i, item in enumerate(pairs):
        unigram = item[0]
        outFile.write(str(i+1) + ":" + " "*(spaces2-len(str(i+1))) + unigram + " " * (spaces-len(unigram)) + str(item[1]) + "\n")

def getUnigrams(words, outFile, numToShow=25, ignore_stopwords=True):
    if ignore_stopwords:
        words = [w for w in words if w not in ignored_words]

    outFile.write("Total words :" + str(len(words)) + "\n")

    countsMap = {}

    for word in words:
        if word not in countsMap:
            countsMap[word] = 0
        countsMap[word] = countsMap[word] + 1

    counts = set()

    for key in countsMap.keys():
        counts.add((key, countsMap[key]))

    outFile.write("Unique words :" + str(len(counts)) + "\n")

    best_freq = sorted(counts, key=lambda x: -x[1])[:numToShow]
    outFile.write("\nFrequency\n")
    printPairs(best_freq, outFile)


positive = []
negative = []

dir = "../project_materials/mypersonality_depression/text/"

for i, user in enumerate(users_negative):
    negative += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break
for i, user in enumerate(users_positive):
    positive += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break

outFilePositive.write("POSITIVE:\n")
getUnigrams(positive, outFilePositive, numToShow=1000, ignore_stopwords=noStopWords)
outFileNegative.write("NEGATIVE\n")
getUnigrams(negative, outFileNegative, numToShow=1000, ignore_stopwords=noStopWords)

outFilePositive.close()
outFileNegative.close()