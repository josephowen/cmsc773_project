import nltk
from nltk.collocations import *
import string
import os
import sys
import csv
import warnings
warnings.filterwarnings("ignore")

usersFilename = "../users.train.csv"

users_positive = []
users_negative = []

with open(usersFilename, 'rb') as f:
    reader = csv.reader(f, delimiter=",")
    first = True
    for row in reader:
        if not first:
            if row[-1] == "1":
                users_positive.append(row[0])
            else:
                users_negative.append(row[0])
        else:
            first = False


ignored_words = nltk.corpus.stopwords.words('english')

noStopWords = False

if len(sys.argv) > 1:
    if len(sys.argv) == 3:
        fileBase = sys.argv[1]
        dot = fileBase.rfind(".")

        outFilePositive = open(fileBase[:dot]+"_positive"+fileBase[dot:], 'w')
        outFileNegative = open(fileBase[:dot]+"_negative"+fileBase[dot:], 'w')

        if sys.argv[2] == "nostopwords":
            noStopWords = True
    else:
        if sys.argv[1] == "nostopwords":
            noStopWords = True
        else:
            fileBase = sys.argv[1]
            dot = fileBase.rfind(".")

            outFilePositive = open(fileBase[:dot]+"_positive"+fileBase[dot:], 'w')
            outFileNegative = open(fileBase[:dot]+"_negative"+fileBase[dot:], 'w')
else:
    outFilePositive = sys.stdout
    outFileNegative = sys.stdout

emotes=[":)", "(:", ":(", "):",
        "c:", ":c",
        "=)", "(=", "=(", ")=",
        ":-)", "(-:", ":-(", ")-:",
        ":-c", "c-:",
        ";)", "(;", ";(", ");",
        ";-)", "(-;", ";-(", ")-;",
        ":'(", ")':", ":'c",
        ">:(", "):<", ">:c",
        ">=(", ")=<", ">=c",
        ":d", "d:", "=d", "d=",
        ":p", "p:", ";p", "p;",
        "o_o", "o.o",
        "^_^", "^.^", "^^",
        ">.<", ">_<", "><",
        ">.>", "<.<", ">_>", "<_<",
        ":3", "=3",
        "-.-", "-_-",
        ":]", "[:", ":[", "]:",
        "=]", "[=", "=[", "]=",
        ":|", "|:", "=|", "|=",
        "<3"]

def getDocWords(file):
    doc = []
    with open(file) as f:
        for line in f:
            thisLine = []
            words = line.split()
            for word in words:
                if not word in string.punctuation:
                    word = word.lower()

                    #thisLine.append(word.lower())
                    
                    if word in emotes:
                        thisLine.append(word)
                    else:
                        for char in string.punctuation:
                            word = word.strip(char)
                        if len(word) != 0:
                            thisLine.append(word)
                    
            doc.append(thisLine)
    return doc

def printPairs(pairs, outFile):
    maxWord = 0
    maxNum = 0
    for i, item in enumerate(pairs):
        maxWord = max(maxWord, len(item[0][0] + " " + item[0][1]))
        maxNum = max(maxNum, len(str(i)))

    spaces = maxWord + 3
    spaces2 = maxNum + 3
    for i, item in enumerate(pairs):
        bigram = item[0][0] + " " + item[0][1]
        outFile.write(str(i+1) + ":" + " "*(spaces2-len(str(i+1))) + bigram + " " * (spaces-len(bigram)) + str(item[1]) + "\n")

def getBigrams(docs, outFile, freqFilter=0, numToShow=25, ignore_stopwords=True):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_documents(docs)

    if ignore_stopwords:
        finder.apply_word_filter(lambda w: w in ignored_words)

    if freqFilter != 0:
        finder.apply_freq_filter(freqFilter)
    best_freq = finder.score_ngrams(bigram_measures.raw_freq)[:numToShow]
    for i, item in enumerate(best_freq):
        best_freq[i] = (item[0], finder.ngram_fd[item[0]])
    outFile.write("\nFrequency\n")
    printPairs(best_freq, outFile)

    best_pmi = finder.score_ngrams(bigram_measures.pmi)[:numToShow]
    outFile.write("\nPMI\n")
    printPairs(best_pmi, outFile)


positive = []
negative = []

dir = "../project_materials/mypersonality_depression/text/"

for i, user in enumerate(users_negative):
    negative += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break
for i, user in enumerate(users_positive):
    positive += getDocWords(dir+user+".txt")
    #if i == 20:
    #    break

outFilePositive.write("POSITIVE:\n")
getBigrams(positive, outFilePositive, freqFilter=50, numToShow=100, ignore_stopwords=noStopWords)
outFileNegative.write("NEGATIVE\n")
getBigrams(negative, outFileNegative, freqFilter=50, numToShow=100, ignore_stopwords=noStopWords)

outFilePositive.close()
outFileNegative.close()