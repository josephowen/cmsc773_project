import re
import os
import sys
import json
import csv
from sets import Set
from math import *
from util import *
from string import *
import nltk
from nltk.collocations import *
import codecs
import pdb

#vw = os.path.join('.', 'vw.exe')
vw = os.path.join('.', 'vw')
usersFilename_train = os.path.join('..', 'users.train.csv')
usersFilename_test = os.path.join('..', 'users.test.csv')

cache = os.path.join('.', 'cache')

emotes=[":)", "(:", ":(", "):",
        "c:", ":c",
        "=)", "(=", "=(", ")=",
        ":-)", "(-:", ":-(", ")-:",
        ":-c", "c-:",
        ";)", "(;", ";(", ");",
        ";-)", "(-;", ";-(", ")-;",
        ":'(", ")':", ":'c",
        ">:(", "):<", ">:c",
        ">=(", ")=<", ">=c",
        ":d", "d:", "=d", "d=",
        ":p", "p:", ";p", "p;",
        "o_o", "o.o",
        "^_^", "^.^", "^^",
        ">.<", ">_<", "><",
        ">.>", "<.<", ">_>", "<_<",
        ":3", "=3",
        "-.-", "-_-",
        ":]", "[:", ":[", "]:",
        "=]", "[=", "=[", "]=",
        ":|", "|:", "=|", "|=",
        "<3"]

featureDict = {}

def getUser(header, row):
    thisUser = {}
    i = 0
    for feature in header[:-1]:
        thisUser[feature] = row[i]
        i += 1
    thisUser["class"] = int(row[-1])
    featureDict[thisUser["userid"]] = Counter()
    return thisUser

def getStatuses():
    users_train = []
    users_test = []

    # Get users from both sets
    with open(usersFilename_train, 'rb') as f:
        reader = csv.reader(f, delimiter=",")
        header = reader.next()
        for row in reader:
            users_train.append(getUser(header, row))

    with open(usersFilename_test, 'rb') as f:
        reader = csv.reader(f, delimiter=",")
        header = reader.next()
        for row in reader:
            users_test.append(getUser(header, row))

    # Get posts from both sets
    
    dir = os.path.join('..', 'project_materials', 'mypersonality_depression', 'text')

    for user in users_train:
        user["posts"] = getPosts(os.path.join(dir, user["userid"]+".txt"))
    for user in users_test:
        user["posts"] = getPosts(os.path.join(dir, user["userid"]+".txt"))

    return users_train, users_test


def getPosts(file):
    posts = []
    with open(file) as f:
        for line in f:
            posts.append(line)
    return posts


folder = os.path.join('.', 'classified')

# global variables
stopSet = Set(nltk.corpus.stopwords.words('english'))
bigram_measures = nltk.collocations.BigramAssocMeasures()
trigram_measures = nltk.collocations.TrigramAssocMeasures()
collocationFinders = (None, None, None, None)

# run 10-fold cross validation
def run10FoldExperiment(filePrefix=os.path.join(folder, 'status_vw'), quietVW=True, divideData=1):
    trainingCorpus, testingCorpus = getStatuses()
    avgF = 0
    avgAcc = 0
    avgAUC = 0

    if divideData > 1:
        print 'dividing data by ' + str(divideData)
        if hasFile:
            output.write('dividing data by ' + divideData)
        trainingCorpus = trainingCorpus[:len(trainingCorpus)/10]
    chunkSize = len(trainingCorpus) / 10

    # Read in CSV feature files
    getCSVFeatures(os.path.join('..', 'hmm', 'hmm.train.csv'))
    getCSVFeatures(os.path.join('..', 'hmm', 'hmm.test.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldafreq.train.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldafreq.test.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldaprob.train.csv')) 
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldaprob.test.csv'))
    getCSVFeatures(os.path.join('..', 'theano', 'word2vec.csv'))

    for i in range(10):
        trainFileVW = filePrefix + str(i) + '_10fold.tr'
        testFileVW  = filePrefix + str(i) + '_10fold.te'
        modelFileVW = filePrefix + str(i) + '_10fold.model'
        rocFile = filePrefix + str(i) + '_10fold.roc.csv'
        
        print 'data split ' + str(i)
        if hasFile:
            output.write('data split ' + str(i) + "\n")
        trainUsers = trainingCorpus[:i*chunkSize] + trainingCorpus[(i + 1)*chunkSize:]
        testUsers = trainingCorpus[i*chunkSize:(i + 1)*chunkSize]
        
        print 'generating classification data ' + str(i)
        if hasFile:
            output.write('generating classification data ' + str(i) + "\n")
        generateVWData(trainUsers, trainFileVW)
        generateVWData(testUsers, testFileVW)

        print 'training model ' + str(i)
        if hasFile:
            output.write('training model ' + str(i) + "\n")
        trainVW(trainFileVW, modelFileVW, quietVW)

        print 'testing model ' + str(i)
        if hasFile:
            output.write('testing model ' + str(i) + "\n")
        (pred, scores) = testVW(testFileVW, modelFileVW, quietVW)
        
        print 'evaluating predictions ' + str(i)
        (F1, acc) = evaluatePredictions(testUsers, pred)
        (points, AUC) = evaluateROC(testUsers, scores)
        print 'f1 score ' + str(i) + ': ' + str(F1)
        print 'accuracy ' + str(i) + ': ' + str(acc)
        print 'auc ' + str(i) + ': ' + str(AUC)
        if hasFile:
            output.write('f1 score ' + str(i) + ': ' + str(F1) + "\n")
            output.write('accuracy ' + str(i) + ': ' + str(acc) + "\n")
            output.write('auc ' + str(i) + ': ' + str(AUC) + "\n")
        
        with open(rocFile, 'w') as h:
            h.write("fpr, tpr\n")
            for (fpr, tpr) in points:
                h.write(str(fpr) + ", " + str(tpr) + "\n")
        
        avgF += F1
        avgAcc += acc
        avgAUC += AUC

    return (avgF / float(10), avgAcc / float(10), avgAUC / float(10))

# run full experiment
def runFullExperiment(filePrefix=os.path.join(folder, 'status_vw'), quietVW=True):
    trainingCorpus, testingCorpus = getStatuses()

    trainFileVW = filePrefix + '.tr'
    testFileVW  = filePrefix + '.te'
    modelFileVW = filePrefix + '.model'
    rocFile = filePrefix + '.roc.csv'
    
    trainUsers = trainingCorpus
    testUsers = testingCorpus
    
    # Read in CSV feature files
    getCSVFeatures(os.path.join('..', 'hmm', 'hmm.train.csv'))
    getCSVFeatures(os.path.join('..', 'hmm', 'hmm.test.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldafreq.train.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldafreq.test.csv'))
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldaprob.train.csv')) 
    getCSVFeatures(os.path.join('..', 'SeqLDA', 'ldaprob.test.csv'))
    getCSVFeatures(os.path.join('..', 'theano', 'word2vec.csv'))
    
    print 'generating classification data'
    if hasFile:
        output.write('generating classification data\n')
    generateVWData(trainUsers, trainFileVW)
    generateVWData(testUsers, testFileVW)

    print 'training model'
    if hasFile:
        output.write('training model\n')
    trainVW(trainFileVW, modelFileVW, quietVW)

    print 'testing model'
    if hasFile:
        output.write('testing model\n')
    (pred, scores) = testVW(testFileVW, modelFileVW, quietVW)
    
    print 'evaluating predictions'
    (F1, acc) = evaluatePredictions(testUsers, pred)
    (points, AUC) = evaluateROC(testUsers, scores)
    print 'f1 score: ' + str(F1)
    print 'accuracy: ' + str(acc)
    print 'auc: ' + str(AUC)
    if hasFile:
        output.write('f1 score: ' + str(F1) + "\n")
        output.write('accuracy: ' + str(acc) + "\n")
        output.write('auc: ' + str(AUC) + "\n")
    
    with open(rocFile, 'w') as h:
        h.write("fpr, tpr\n")
        for (fpr, tpr) in points:
            h.write(str(fpr) + ", " + str(tpr) + "\n")

    return (F1, acc, AUC)

def getUserWords(posts):
    doc = []
    for line in posts:
        thisLine = []
        words = line.split()
        for word in words:
            if not word in punctuation:
                word = word.lower()
                
                if not word in emotes:
                    for char in punctuation:
                        word = word.strip(char)

                if len(word) != 0:
                    thisLine.append(word)
        doc.append(thisLine)
    return doc

bigram_measures = nltk.collocations.BigramAssocMeasures()
trigram_measures = nltk.collocations.TrigramAssocMeasures()

def getCollocationFinders(posts):
    statusWords = []
    statusWords += getUserWords(posts)
    return BigramCollocationFinder.from_documents(statusWords), TrigramCollocationFinder.from_documents(statusWords)

def getFirstWords(user, firstN=3, num=10, readCache=True, writeCache=True):
    userPath = os.path.join(cache, "user_"+user["userid"]+"_firsts_" + str(firstN) + "_" + str(num) + ".txt")
    if readCache:
        if os.path.exists(userPath):
            try:
                with open(userPath, 'r') as f:
                    fromCache = json.load(f)
                firstWords = fromCache
                return firstWords
            except:
                print "Error loading from cache", userPath
                pass

    words = getUserWords(user["posts"])
    words = [item for sublist in words for item in sublist[:firstN]]

    numWords = len(words)
    
    countsMap = {}
    for word in words:
        if word not in countsMap:
            countsMap[word] = 0
        countsMap[word] = countsMap[word] + 1
    counts = set()
    for key in countsMap.keys():
        counts.add((key, countsMap[key]))

    firstWords = sorted(counts, key=lambda x: -x[1])[:num]

    firstWords_valid = []
    for i, item in enumerate(firstWords):
        try:
            firstWords_valid.append((item[0].encode('latin-1').decode('utf8'), item[1], i, item[1]/float(numWords)))
        except:
            pass

    if writeCache:
        toCache = firstWords_valid
        try:
            with open(userPath, 'w') as f:
                json.dump(toCache, f)
        except:
            print "Error writing to cache:", toCache

    return firstWords_valid

def getLastWords(user, lastN=3, num=10, readCache=True, writeCache=True):
    userPath = os.path.join(cache, "user_"+user["userid"]+"_lasts_" + str(lastN) + "_" + str(num) + ".txt")
    if readCache:
        if os.path.exists(userPath):
            try:
                with open(userPath, 'r') as f:
                    fromCache = json.load(f)
                firstWords = fromCache
                return firstWords
            except:
                print "Error loading from cache", userPath
                pass

    words = getUserWords(user["posts"])
    words = [item for sublist in words for item in sublist[lastN:]]

    numWords = len(words)
    
    countsMap = {}
    for word in words:
        if word not in countsMap:
            countsMap[word] = 0
        countsMap[word] = countsMap[word] + 1
    counts = set()
    for key in countsMap.keys():
        counts.add((key, countsMap[key]))

    firstWords = sorted(counts, key=lambda x: -x[1])[:num]

    firstWords_valid = []
    for i, item in enumerate(firstWords):
        try:
            firstWords_valid.append((item[0].encode('latin-1').decode('utf8'), item[1], i, item[1]/float(numWords)))
        except:
            pass

    if writeCache:
        toCache = firstWords_valid
        try:
            with open(userPath, 'w') as f:
                json.dump(toCache, f)
        except:
            print "Error writing to cache:", toCache

    return firstWords_valid

#unigrams
def getUnigrams(user, freqFilter=5, num=10, ignoreStopWords=False, readCache=True, writeCache=True):
    userPath = os.path.join(cache, "user_"+user["userid"]+"_unigrams_" + str(freqFilter) + "_" + str(num) + "_" + str(ignoreStopWords) + ".txt")
    if readCache:
        if os.path.exists(userPath):
            try:
                with open(userPath, 'r') as f:
                    fromCache = json.load(f)
                unigram_best_freq = fromCache
                return unigram_best_freq
            except:
                print "Error loading from cache", userPath
                pass

    words = getUserWords(user["posts"])
    words = [item for sublist in words for item in sublist]
    countsMap = {}

    if ignoreStopWords:
        words = [w for w in words if w not in stopSet]

    numWords = len(words)

    for word in words:
        if word not in countsMap:
            countsMap[word] = 0
        countsMap[word] = countsMap[word] + 1

    counts = set()

    for key in countsMap.keys():
        counts.add((key, countsMap[key]))

    unigram_best_freq = sorted(counts, key=lambda x: -x[1])[:num]

    #print "\nBefore:", unigram_best_freq

    unigram_best_freq_valid = []
    for i, item in enumerate(unigram_best_freq):
        if unigram_best_freq[i][1] < freqFilter:
            continue
        try:
            unigram_best_freq_valid.append((item[0].encode('latin-1').decode('utf8'), item[1], i, item[1]/float(numWords)))
        except:
            pass

    #print "After:", unigram_best_freq_valid

    if writeCache:
        toCache = unigram_best_freq_valid
        try:
            with open(userPath, 'w') as f:
                json.dump(toCache, f)
        except:
            print "Error writing to cache:", toCache

    return unigram_best_freq_valid

#Returns ngrams in the form (ngram, frequency, nltk-calculated-value, rank, normalized-frequency)
def getCollocations(user, freqFilter=5, num=10, ignoreStopWords=False, readCache=True, writeCache=True):
    userPath = os.path.join(cache, "user_"+user["userid"]+"_collocations_" + str(freqFilter) + "_" + str(num) + "_" + str(ignoreStopWords) + ".txt")
    if readCache:
        if os.path.exists(userPath):
            try:
                with open(userPath, 'r') as f:
                    fromCache = json.load(f)
                bigram_best_freq = fromCache['bigrams']
                trigram_best_freq = fromCache['trigrams']
                return bigram_best_freq, trigram_best_freq
            except:
                print "Error loading from cache", userPath
                pass


    posts = user["posts"]
    numWords = 0
    for post in posts:
        numWords += len(post)
    bigramFinder, trigramFinder = getCollocationFinders(posts)
    
    if ignoreStopWords:
        bigramFinder.apply_word_filter(lambda w: w in stopSet)
        trigramFinder.apply_word_filter(lambda w: w in stopSet)

    if freqFilter != 5:
        bigramFinder.apply_freq_filter(freqFilter)
        trigramFinder.apply_freq_filter(freqFilter)

    bigram_best_freq = bigramFinder.score_ngrams(bigram_measures.raw_freq)[:num]
    trigram_best_freq = trigramFinder.score_ngrams(trigram_measures.raw_freq)[:num]
    bigram_best_freq_valid = []
    trigram_best_freq_valid = []
    for i, item in enumerate(bigram_best_freq):
        try:
            gram = (item[0][0].encode('latin-1').decode('utf8'), item[0][1].encode('latin-1').decode('utf8'))
            freq = bigramFinder.ngram_fd[item[0]]
            bigram_best_freq_valid.append((gram, freq, item[1], i, freq/float(numWords)))
        except:
            pass
    for i, item in enumerate(trigram_best_freq):
        try:
            gram = (item[0][0].encode('latin-1').decode('utf8'), item[0][1].encode('latin-1').decode('utf8'), item[0][2].encode('latin-1').decode('utf8'))
            freq = trigramFinder.ngram_fd[item[0]]
            trigram_best_freq_valid.append((gram, freq, item[1], i, freq/float(numWords)))
        except:
            pass

    if writeCache:
        toCache = {"bigrams": bigram_best_freq_valid, "trigrams": trigram_best_freq_valid}
        try:
            with open(userPath, 'w') as f:
                json.dump(toCache, f)
        except:
            print "Error writing to cache:", toCache

    return bigram_best_freq_valid, trigram_best_freq_valid


# create vw data file
def generateVWData(users, trainFile):
    with open(trainFile, 'w') as f:
        for user in users:
            f.write(str(user["class"]))
            f.write(" ")
            f.write("|f")
            getUserFeatures(user)
            features = featureDict[user["userid"]]
            for k,v in features.iteritems():
                f.write(" ")
                f.write(str(k).replace(" ", "{space}").replace(":", "{colon}").replace("-", "{hyphen}").replace("|", "{pipe}"))
                f.write(":")
                f.write(str(v))
            f.write("\n")

def getCSVFeatures(csvFile):
    with open(csvFile, 'rb') as f:
        reader = csv.reader(f, delimiter=",")
        header = reader.next()
        for row in reader:
            id = row[0]
            features = featureDict[id]
            for feat, val in zip(header[1:-1], row[1:-1]):
                features[feat] = val

# get user features
def getUserFeatures(user):
    username = user["userid"]
    posts = user["posts"]
    
    features = featureDict[username]

    #features["score" + str(user["score"])] = 1
    #features["score"] =  int(user["score"])
    #features["baseline"+str(user["class"])] = 1
    
    features["completion_time" + user["completion_time"]] = 1
    features["ethnicity_" + user["ethnicity"]] = 1
    #~ features["marital_status_" + user["marital_status"]] = 1
    features["parents_together_" + user["parents_together"]] = 1
    #~ features["num_posts"] = len(posts)

    firstWords = getFirstWords(user)
    if len(firstWords) > 0 and len(firstWords[0]) != 4:
        firstWords = getFirstWords(user, readCache=False)
    for firstWord in firstWords:
        #features["first_freq_"+str(firstWord[0])] = firstWord[1]
        #features["first_rank_"+str(firstWord[0])] = firstWord[2]
        features["first_norm_"+str(firstWord[0])] = firstWord[3]

    '''
    lastWords = getLastWords(user)
    if len(lastWords) > 0 and len(lastWords[0]) != 4:
        lastWords = getFirstWords(user, readCache=False)
    for lastWord in lastWords:
        #features["last_freq_"+str(lastWord[0])] = lastWord[1]
        #features["last_rank_"+str(lastWord[0])] = lastWord[2]
        features["last_norm_"+str(lastWord[0])] = lastWord[3]
    '''

    #Unigram features
    unigrams = getUnigrams(user)
    if len(unigrams) > 0 and len(unigrams[0]) != 4:
        unigrams = getUnigrams(user, readCache=False)
    for unigram in unigrams:
        #features["unigram_freq_" + str(unigram[0])] = unigram[1]
        features["unigram_rank_" + str(unigram[0])] = unigram[2]
        #features["unigram_norm_" + str(unigram[0])] = unigram[3]

    # collocation features
    (bigrams, trigrams) = getCollocations(user)
    if (len(bigrams) > 0 and len(bigrams[0]) != 5) or (len(trigrams) > 0 and len(trigrams[0]) != 5):
        (bigrams, trigrams) = getCollocations(user, readCache=False)
    for bigram in bigrams:
        #features["bigram_freq_" + str(bigram[0])] = bigram[1]
        #features["bigram_value_" + str(bigram[0])] = bigram[2]
        features["bigram_rank_" + str(bigram[0])] = bigram[3]
        #features["bigram_norm_" + str(bigram[0])] = bigram[4]
    for trigram in trigrams:
        #features["trigram_freq_" + str(trigram[0])] = trigram[1]
        #features["trigram_value_" + str(trigram[0])] = trigram[2]
        features["trigram_rank_" + str(trigram[0])] = trigram[3]
        #features["trigram_norm_" + str(trigram[0])] = trigram[4]

# will be 10-fold cross validation
def trainVW(dataFilename, modelFilename, quietVW=True):
    cmd = vw + ' -k -c -b 25 --holdout_off --passes 10 -d ' + dataFilename + ' -f ' + modelFilename
    # -q st -l 0.5 --initial_t 0.5 --power_t 0.5 --decay_learning_rate 0.5
    if quietVW: cmd += ' --quiet'
    print 'executing: ' + str(cmd)
    if hasFile:
        output.write('executing: ' + str(cmd) + "\n")
    p = os.system(cmd)
    if p != 0:
        raise Exception('execution of vw failed!  return value=' + str(p))

def testVW(dataFilename, modelFilename, quietVW=True):
    cmd = vw + ' -t -d ' + dataFilename + ' -i ' + modelFilename + ' -p ' + dataFilename + '.predictions'
    if quietVW: cmd += ' --quiet'
    print 'executing: ' + str(cmd)
    if hasFile:
        output.write('executing: ' + str(cmd) + "\n")
    p = os.system(cmd)
    if p != 0:
        raise Exception('execution of vw failed!  return value=' + str(p))
    
    predictions = []
    scores = []
    with open(dataFilename + '.predictions') as h:
        for line in h:
            number = float(line.strip())
            scores.append(number)
            if number < 0.5:
                predictions.append(0)
            else:
                predictions.append(1)

    return (predictions, scores)

# compute f-score from predictions
def evaluatePredictions(users, pred):
    # compare length of users and predictions
    length = len(users)
    if len(pred) != length:
        raiseNotDefined()
    
    # true and false positives and negatives
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    for i in range(length):
        predicted = pred[i]
        actual = users[i]["class"]
        if predicted == 1:
            if actual == 1:
                TP += 1
            else:
                FP += 1
        else:
            if actual == 1:
                FN += 1
            else:
                TN += 1
    
    # calculate f-score and accuracy
    F1 = 2*TP / float(2*TP + FP + FN)
    acc = (TP + TN) / float(TP + FP + TN + FN)
    return (F1, acc)

# Trapezoid area to be used by getAUC
def trapezoid_area(x1, x2, y1, y2):
    base = abs(x1 - x2)
    height = (y1 + y2) / 2.0
    return base*height

def evaluateROC(users, scores):
    P = N = 0 # Get positive and negative examples
    examples = []
    i = 0
    for user in users:
        val = user['class']
        if (val == 1):
            P += 1
        else:
            N += 1
        examples.append({'id':user['userid'], 'class':val, 'score':scores[i]})
        i += 1
    examples_sorted = sorted(examples, key=lambda example: example['score'], reverse=True)
    FP = TP = 0
    R = []
    FP_prev = TP_prev = 0
    A = 0
    score_prev = float("-inf")
    for i in range(len(examples_sorted)):
        if scores[i] != score_prev:
            R.append((FP / float(N), TP / float(P)))
            A += trapezoid_area(FP, FP_prev, TP, TP_prev)
            score_prev = scores[i]
            FP_prev = FP
            TP_prev = TP
        if examples_sorted[i]["class"] == 1:
            TP += 1
        else:
            FP += 1
    R.append((FP / float(N), TP / float(P)))
    A += trapezoid_area(N, FP_prev, N, TP_prev)
    A /= (P*N)
    return (R, A)

if __name__ == "__main__":
    if not os.path.exists(folder):
        os.mkdir(folder)
    if not os.path.exists(cache):
        os.mkdir(cache)

    #if false then 10-fold
    fullExperiment = True

    hasFile = False

    if len(sys.argv) > 1:
        if len(sys.argv) == 3:
            if sys.argv[1] == "10fold":
                fullExperiment = False

            hasFile = True
            output = open(sys.argv[2], 'w')
        else:
            if sys.argv[1] == "10fold":
                fullExperiment = False
            else:
                hasFile = True
                output = open(sys.argv[1], 'w')

    if fullExperiment:
        print "Running full experiement"
        (F1, acc, AUC) = runFullExperiment()
    else:
        print "Running 10-fold experiment"
        (F1, acc, AUC) = run10FoldExperiment()
    print "average f-score: " + str(F1)
    print "average accuracy: " + str(acc)
    print "average AUC: " + str(AUC)
    if hasFile:
        output.write("average f-score: " + str(F1) + "\n")
        output.write("average accuracy: " + str(acc) + "\n")
        output.write("average AUC: " + str(AUC) + "\n")

    if hasFile:
        output.close()
