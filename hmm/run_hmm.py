import os
import pdb

def add_common_lines1(h, num_states):
    h.write('# Type of the model\n')
    h.write('model_type hmm\n')
    h.write('\n')
    h.write('# Number of hidden states\n')
    h.write('num_states ' + str(num_states) + '\n')
    h.write('\n')
    h.write('# Emission distribution\n')
    h.write('emission\n')
    h.write('independent 64 bernoulli 2\n')
    h.write('\n')

def add_common_lines2(h, num_data_sequences, data_sequence_length_distinct):
    h.write('# Number of finite-valued vector components for the data\n')
    h.write('num_discrete_data_components 64\n')
    h.write('\n')
    h.write('# Number of output data sequences\n')
    h.write('num_data_sequences ' + str(num_data_sequences) + '\n')
    h.write('\n')
    h.write('# Length of each sequence\n')
    h.write('data_sequence_length_distinct ' + ' '.join(data_sequence_length_distinct) + '\n')
    h.write('\n')

def create_viterbi_file(filename, num_data_sequences, data_sequence_length_distinct, num_states, data_file):
    with open(filename, 'w') as h:
        h.write('# Action\n')
        h.write('action viterbi\n')
        h.write('\n')
        add_common_lines1(h, num_states)
        h.write('# Data file\n')
        h.write('data ' + data_file + '\n')
        h.write('\n')
        h.write('# Model file\n')
        h.write('model_filename ' + os.path.join('.', 'hmm.' + str(num_states) + '.model') + '\n')
        h.write('\n')
        add_common_lines2(h, num_data_sequences, data_sequence_length_distinct)
        h.write('# Output file\n')
        h.write('output ' + data_file + '.viterbi\n')
        h.write('\n')
        h.write('em_verbose\n')
        h.write('\n')

def create_learn_file(filename, num_data_sequences, data_sequence_length_distinct, num_states, data_file):
    with open(filename, 'w') as h:
        h.write('# Action\n')
        h.write('action learn\n')
        h.write('\n')
        add_common_lines1(h, num_states)
        h.write('# Data file\n')
        h.write('data ' + data_file + '\n')
        h.write('\n')
        add_common_lines2(h, num_data_sequences, data_sequence_length_distinct)
        h.write('# Output file\n')
        h.write('output ' + os.path.join('.', 'hmm.' + str(num_states) + '.model') + '\n')
        h.write('\n')
        h.write('# Number of random restarts\n')
        h.write('num_restarts 10\n')
        h.write('\n')
        h.write('em_verbose\n')
        h.write('\n')

if __name__ == "__main__":
    # Get data paths
    path_train = os.path.join('..', 'project_materials', 'hmm', 'train')
    path_test = os.path.join('..', 'project_materials', 'hmm', 'test')
    path_text_in = os.path.join('..', 'project_materials', 'mypersonality_depression', 'text')
    
    # Get output paths
    path_hmm_all = os.path.join('.', 'hmm.in')
    path_hmm_train = os.path.join('.', 'hmm.train')
    path_hmm_test = os.path.join('.', 'hmm.test')
    path_text_out = os.path.join('..', 'project_materials', 'hmm', 'text')
    if not os.path.exists(path_text_out):
        os.mkdir(path_text_out)
    
    # Counts
    train_sequences = test_sequences = 0
    train_length_distinct = []
    test_length_distinct = []
    
    # Create combined HMM file
    with open(path_hmm_all, 'w') as h:
        with open(os.path.join(path_text_out, 'text.in'), 'w') as outFile:
            for filename in os.listdir(path_train):
                train_sequences += 1
                with open(os.path.join(path_train, filename), 'r') as f:
                    for i, l in enumerate(f):
                        h.write(l)
                train_length_distinct.append(str(i + 1))
                with open(os.path.join(path_text_in, filename), 'r') as inFile:
                    outFile.write(inFile.read())
            for filename in os.listdir(path_test):
                test_sequences += 1
                with open(os.path.join(path_test, filename), 'r') as f:
                    for i, l in enumerate(f):
                        h.write(l)
                test_length_distinct.append(str(i + 1))
                with open(os.path.join(path_text_in, filename), 'r') as inFile:
                    outFile.write(inFile.read())

    # Create learn file
    num_states = 51
    create_learn_file(os.path.join('.', 'learn_hmm_independent'), train_sequences + test_sequences, train_length_distinct + test_length_distinct, num_states, path_hmm_all)
    create_viterbi_file(os.path.join('.', 'viterbi_hmm_independent'), train_sequences + test_sequences, train_length_distinct + test_length_distinct, num_states, path_hmm_all)

    # Create train and test files
    with open(path_hmm_train, 'w') as h:
        for filename in os.listdir(path_train):
            with open(os.path.join(path_train, filename), 'r') as f:
                h.write(f.read())
    create_viterbi_file(os.path.join('.', 'viterbi_hmm_train'), train_sequences, train_length_distinct, num_states, path_hmm_train)
    
    with open(path_hmm_test, 'w') as h:
        for filename in os.listdir(path_test):
            with open(os.path.join(path_test, filename), 'r') as f:
                h.write(f.read())
    create_viterbi_file(os.path.join('.', 'viterbi_hmm_test'), test_sequences, test_length_distinct, num_states, path_hmm_test)
