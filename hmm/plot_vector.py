from matplotlib import pyplot
import matplotlib as mpl
import numpy as np
import operator
import os
import pdb
import re

def plot_vectors(vecs):
    # Make color map from red to green
    cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['green', 'yellow', 'red'], 256)

    # Tell imshow about color map so only set colorsused
    img = pyplot.imshow(vecs, interpolation='nearest', cmap=cmap, origin='lower')

    # Make color bar
    pyplot.colorbar(img, cmap=cmap)
    
    # Titles and labels
    pyplot.title('Emission Probabilities for each State')
    pyplot.xlabel('LIWC Categories')
    pyplot.ylabel('States')

    # Show plot
    pyplot.show()

def get_vector(lines):
    vec = np.array([0.0]*64)
    for i in range(len(lines)):
        probs = lines[i].split()
        vec[i] = float(probs[1])
    return vec

def get_vectors_from_model(filename):
    vecs = []
    prog = re.compile('# State #([\d]+):')
    with open(filename, 'r') as h:
        lines = list(h)
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        if prog.match(line) is None:
            i += 1
        else:
            vecs.append(get_vector(lines[(i + 1):(i + 65)]))
            i += 65
    return np.array(vecs)

def get_vectors_from_input(filename):
    vecs = []
    with open(filename, 'r') as h:
        for line in h:
            vecs.append(np.array([int(x) for x in line.strip().split()]))
    return np.array(vecs)

def analyze_vectors(vecs):
    RSS = {}
    for i in range(len(vecs)):
        for j in range(i + 1, len(vecs)):
            RSS[(i, j)] = np.sum((vecs[i] - vecs[j])**2)
    sorted_RSS = sorted(RSS.items(), key=operator.itemgetter(1))
    return sorted_RSS

if __name__ == '__main__':
    # Get state vectors from model file
    filename = os.path.join('.', 'hmm.51.model')
    vecs = get_vectors_from_model(filename)
    
    # Get state vectors from depressed file
    #~ filename = os.path.join('.', 'hmm.depressed')
    #~ vecs = get_vectors_from_input(filename)
    
    # Analyze vectors
    #~ sorted_RSS = analyze_vectors(vecs)
    
    # Plot vector
    #~ (first, second) = sorted_RSS[0][0]
    #~ plot_vectors(np.array([vecs[first], vecs[second]]))
    plot_vectors(vecs)

    