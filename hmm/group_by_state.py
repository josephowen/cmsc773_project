import csv
import os
import pdb

# Get user IDs from CSV
def getUsers(filename):
    users = {}
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        header = reader.next()
        for row in reader:
            users[row[0]] = row[-1]
    return users

def getLineCounts(line):
    num_states = 51
    states = [0] * num_states
    for state in line.split():
        states[int(state)] += 1
    #~ sum = 0
    #~ for num in states:
        #~ sum += num
    #~ for i in range(num_states):
        #~ states[i] /= float(sum)
    return states

def createFeatureCSV(users, path_viterbi, path_csv, path_files):
    with open(path_csv, 'w') as h:
        h.write('userid,')
        h.write(','.join(['state' + str(x) for x in range(51)]))
        h.write(',class\n')
        lines = []
        with open(path_viterbi, 'r') as f:
            lines = [x.strip() for x in list(f) if x.strip() != '']
        for userid, line in zip([x.split('.')[0] for x in os.listdir(path_files)], lines):
            h.write(userid + ',')
            h.write(','.join([str(x) for x in getLineCounts(line)]))
            h.write(',' + users[userid] + '\n')

if __name__ == "__main__":
    # Get file paths
    path_text = os.path.join('..','project_materials','hmm','text')
    path_viterbi = os.path.join('.','hmm.in.viterbi')
    path_train = os.path.join('..','project_materials','hmm','train')
    path_test = os.path.join('..','project_materials','hmm','test')
    
    # Get user IDs for training and test sets
    train = getUsers(os.path.join('..','users.train.csv'))
    test = getUsers(os.path.join('..','users.test.csv'))
    
    # Create feature CSV
    createFeatureCSV(train, os.path.join('.','hmm.train.viterbi'), os.path.join('.','hmm.train.csv'), path_train)
    createFeatureCSV(test, os.path.join('.','hmm.test.viterbi'), os.path.join('.','hmm.test.csv'), path_test)
    
    # Read lists from files
    text = []
    states = []
    with open(os.path.join(path_text, 'text.in'), 'r') as h:
        text = list(h)
    text = [x.strip() for x in text]
    with open(path_viterbi, 'r') as h:
        tmp = h.read()
    states = tmp.split()
    
    # Dictionary of statuses by state
    dict = {}
    for i in set(states):
        dict[i] = set()
    for status, state in zip(text, states):
        dict[state].add(status)
    
    # Create files by state
    for i in set(states):
        with open(os.path.join(path_text, 'text.' + str(i) + '.in'), 'w') as h:
            for status in dict[i]:
                h.write(status + '\n')
