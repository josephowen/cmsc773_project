import operator
import os
import pdb

if __name__ == "__main__":
    rows = {}
    
    # Create combined file
    with open(os.path.join('.', 'hmm.in'), 'r') as h:
        for line in h:
            key = line.strip()
            if rows.has_key(key):
                rows[key] += 1
            else:
                rows[key] = 1

    sorted_rows = sorted(rows.items(), key=operator.itemgetter(1), reverse=True)

    pdb.set_trace()
