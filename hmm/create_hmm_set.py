import csv
import os
import pdb
import re
import string
from pytrie import SortedStringTrie

# Dictionary that maps number to category
dict = {}

# Trie that stores prefixes
trie = SortedStringTrie()

# Get user IDs from CSV
def getIds(filename):
    ids = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        header = reader.next()
        for row in reader:
            ids.append(row[0])
    return ids

# Create files used to train HMM
def createFiles(set, dir):
    text_dir = os.path.join('..', 'project_materials', 'mypersonality_depression', 'text')
    if not os.path.exists(dir):
        os.mkdir(dir)
    
    for id in set:
        with open(os.path.join(text_dir, id) + '.txt', 'r') as inFile:
            with open(os.path.join(dir, id) + '.txt', 'w') as outFile:
                for line in inFile:
                    outFile.write(convertLineToLIWC(line.strip()) + '\n')

# Convert line to vector of LIWC categories
def convertLineToLIWC(line, sep=' '):
    # Split line and stuff
    words = string.lower(line).split()
    result = ['0'] * 64
    for word in words:
        i = 0
        while i < len(word) and word[i] in string.punctuation:
            i += 1
        for index in trie.longest_prefix_value(word[i:], default=[]):
            result[dict[index][1]] = '1'
    return sep.join(result)

# Initialize dictionary and trie
def createDict(filename):
    global dict
    global trie
    tmp = {}
    with open(filename, 'r') as f:
        div = f.readline()
        flag = True # flag whether to add to dict or to trie
        index = 0
        for line in f:
            if line == div:
                flag = False
            else:
                l = line.strip().split()
                word = l[0]
                if flag:
                    dict[word] = (l[1], index)
                    index += 1
                else:
                    trie[word] = l[1:]
    return dict

def modifyDict(dir):
    oldFile = os.path.join(liwc_dir, 'LIWC2007.dic')
    newFile = os.path.join(liwc_dir, 'LIWC2007.new.dic')
    with open(oldFile, 'r') as inFile:
        with open(newFile, 'w') as outFile:
            for line in inFile:
                newLine = line.replace("\x92", "'").replace("<of>", "")
                l = re.findall(r"([a-zA-Z0-9_'%]+)", newLine.strip())
                value = ['2' if x == '02' else x for x in l]
                outFile.write('\t'.join(value) + '\n')

def createCSV(set, csvFile):
    text_dir = os.path.join('..', 'project_materials', 'mypersonality_depression', 'text')
    with open(csvFile, 'w') as outFile:
        outFile.write(','.join(['cat' + str(x) for x in range(64)]) + ',class\n')
        for id in set:
            with open(os.path.join(text_dir, id) + '.txt', 'r') as inFile:
                for line in inFile:
                    outFile.write(convertLineToLIWC(line.strip(), sep=','))
                    outFile.write(',1\n' if id in depressed else ',0\n')

if  __name__ =='__main__':
    # Get user IDs for training and test sets
    train = getIds(os.path.join('..', 'users.train.csv'))
    test = getIds(os.path.join('..', 'users.test.csv'))
    
    # Get user IDs for depressed and nondepressed sets
    depressed = getIds(os.path.join('..', 'users.depressed.csv'))
    
    # Create dictionary
    liwc_dir = os.path.join('..', 'project_materials', 'liwc')
    if not os.path.exists(os.path.join(liwc_dir, 'LIWC2007.new.dic')):
        # Modify dictionary for easy reading
        modifyDict(os.path.join(liwc_dir, 'LIWC2007.dic'))
    createDict(os.path.join(liwc_dir, 'LIWC2007.new.dic'))
    
    # Create HMM files
    hmm_dir = os.path.join('..', 'project_materials', 'hmm')
    if not os.path.exists(hmm_dir):
        os.mkdir(hmm_dir)

    # Create train and test HMM directories and files
    createFiles(train, os.path.join(hmm_dir, 'train'))
    createFiles(test, os.path.join(hmm_dir, 'test'))
    
    # Create CSV file for exploratory analysis
    createCSV(train + test, os.path.join(liwc_dir, 'users.liwc.csv'))
