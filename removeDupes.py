import os

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def linesEqual(fname, line1, line2):
    first = ""
    second = ""
    with open(fname) as f:
        for i, l in enumerate(f):
            if i == line1:
                first = l
            elif i == line2:
                second = l
    return first == second

def newFileHalf(oldFileName, newFileName, line):
     with open(oldFileName) as oldFile:
        with open(newFileName, 'w') as newFile:
            for i, l in enumerate(oldFile):
                if i == line:
                    break
                newFile.write(l)

dir = "project_materials/mypersonality_depression/text/"
#dir = "project_materials/mypersonality_neuroticism/text/"
newDir = "project_materials/mypersonality_depression/text_fixed/"

if not os.path.exists(newDir):
    os.mkdir(newDir)

total = 0
dupes = 0

for file in os.listdir(dir):
    fileName = dir+file
    length = file_len(fileName)
    total += 1
    if linesEqual(fileName, 0, length/2):
        dupes += 1
        newFileHalf(fileName, newDir+file, length/2)

print dupes, "duplicates out of", total
